import os
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as mplcm
import matplotlib.colors as colors
import pandas as pd
import datetime
import math
import copy
import re
import glob
import tarfile
import shutil
import logging
import signal
import sys
import gc


class azureusTimeConvert:

    def __init__(self,initTimestamp):
        self.lastTimestamp = copy.copy(initTimestamp)
    
    def setLastTimestamp(self,ts):
        self.lastTimestamp = copy.copy(ts)
    
    def buggytimeconvert(self,time):
    
        # parse time from string
        timeObj = datetime.time(int(time[0:2]),int(time[3:5]),math.floor(float(time[6:])),math.floor((float(time[6:]) - math.floor(float(time[6:])))*1000000))
        # create new timestamp with date from last timestamp
        newTimestamp = self.lastTimestamp.replace(hour=timeObj.hour,minute=timeObj.minute,second=timeObj.second,microsecond=timeObj.microsecond)

        # check whether diff is negative -> indicates wrap from day caused by date change (this won't work for entries with a bigger timespan than 24h)
        diff = newTimestamp - self.lastTimestamp  
        if diff.total_seconds() < 0 :
            if abs(diff) > datetime.timedelta(minutes=10):
                newTimestamp = newTimestamp + datetime.timedelta(days=1)
                diff = newTimestamp - self.lastTimestamp
        
        self.lastTimestamp = newTimestamp
        return newTimestamp


def open_tar_file(path):
    if path.endswith(".tar.gz"):
        try:
            var = tarfile.open(name=path, mode='r:gz')  # returns tarpointer
        except:
            var = False
        return var
    else:
        return False


def read_file_from_tar(tarf, file_to_extract):
    if tarf.getmember(file_to_extract):
        return tarf.extractfile(file_to_extract)        # returns filepointer in tar file
    else:
        return False


def ul_dl_list(filelist):
    matching_ul = [s for s in filelist if "ul_" in s]
    matching_dl = [s for s in filelist if "dl_" in s]
    return matching_ul, matching_dl


def calc_ul_start(matching_ul):
    List = []
    for i in matching_ul:
        help1 = "Galileo"
        help2 = "ul_begin_"
        index = i.find(help2)
        if index != -1:
            ul_node = i.find(help1)
            node_number = i[ul_node:ul_node+9].replace(help1,"")
            node_number = node_number.replace("/","")
            ul_node = help1 + node_number
            print("Upload Node: ", ul_node)
            ul_ready = i[index:-1]
            ul_ready = ul_ready.replace(help2,"")
            ul_start = datetime.datetime.strptime(ul_ready, "%d.%m.%Y.%H.%M.%S")
            print("Upload Start: ", ul_start)
            List.append([ul_node, ul_start])
    return List


def calc_ul_start_from_stats_foldername(foldername):
    foldername_stripped = re.split(r'[-]', foldername)
    foldername_stripped = foldername_stripped[1] + "-" + foldername_stripped[2]
    timestamp = datetime.datetime.strptime(foldername_stripped, "%Y_%m_%d-%H_%M_%S")
    return timestamp

 
def grepLogs(seed, leechers,tarf,tarfname):
       
    fin = read_file_from_tar(tarf, tarfname+"/Galileo{0}/az.log".format(seed))
##    text = str(fin.read(),"utf-8")
##    matches = re.findall(r"(.*)Sent\s\[BT\_PIECE(.*)",text)
##    print(matches)

    foutSent = open(outputPath+"SentPiece"+str(seed)+".log", 'w')

##    for match in matches:
##        foutSent.write(match)
    
    print("Seeder Sent File\n")
    for line in fin:
        
        matchSent = re.search(r"(.*)Sent\s\[BT\_PIECE(.*)", str(line, "utf-8"))
        if matchSent:
            foutSent.write(matchSent.group())
        
    foutSent.close()
    
    for i in leechers:
        
        print("Receive and Sent file from Leecher Galileo" + str(i) )
        fin = read_file_from_tar(tarf, tarfname+"/Galileo{0}/az.log".format(i))
        
        foutReceived = open(outputPath+"ReceivedPiece"+str(i)+".log", 'w')
        foutSent = open(outputPath+"SentPiece"+str(i)+".log", 'w')
        
        for line in fin:
        
            matchReceived = re.search(r"(.*)Received\s\[BT\_PIECE(.*)", str(line, "utf-8"))
            if matchReceived:
                foutReceived.write(matchReceived.group())
            
            matchSent = re.search(r"(.*)Sent\s\[BT\_PIECE(.*)", str(line, "utf-8"))
            if matchSent:
                foutSent.write(matchSent.group())
        
        foutReceived.close()
        foutSent.close()
 
    
# try folder again to check whether files changed in the meantime
somethingWasProcessed = True; 
fastQuitRequested=False;


def signal_handler(signal, frame):
    global fastQuitRequested
    if(fastQuitRequested):
        logging.error("aborted by user")
        sys.exit(0)
    else:
        fastQuitRequested=True
        print("Quit requested, wait until current calc finishes. To immediately quit, press ctrl+c again")


signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)


while(somethingWasProcessed):
    
    # iterate over all archives
    somethingWasProcessed=False
    for tar_file in glob.glob("*.tar.gz"):
        
        # release some memory before starting next calc
        gc.collect()
        tarfname = os.path.splitext(os.path.splitext(tar_file)[0])[0]
        print(tarfname)
        
        # determine initial seed and leechers of current measurement data set using archive name (TIS1/TIS15, LP1-5)
        tarfnameSplit = re.split(r'[.]', tarfname)
        
        if tarfnameSplit[1] == "TIS1":
            seed = int(1)
            if tarfnameSplit[2] == "LP1":
                leechers = [3, 5, 13, 15, 17, 25, 27, 29]
            if tarfnameSplit[2] == "LP2":
                leechers = [3, 5, 8, 10, 13, 15, 17, 20, 22, 25, 27, 29]
            if tarfnameSplit[2] == "LP3":
                leechers = [2, 3, 4, 5, 7, 11, 13, 15, 17, 19, 23, 25, 26, 27, 28, 29]
            if tarfnameSplit[2] == "LP4":
                leechers = [2, 3, 4, 5, 7, 8, 10, 11, 13, 15, 17, 19, 20, 22, 23, 25, 26, 27, 28, 29]
            if tarfnameSplit[2] == "LP5":
                leechers = [2, 3, 4, 5, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 25, 26, 27, 28, 29]
        
        if tarfnameSplit[1] == "TIS15":
            seed = int(15)
            if tarfnameSplit[2] == "LP1":
                leechers = [1, 3, 5, 13, 17, 25, 27, 29]
            if tarfnameSplit[2] == "LP2":
                leechers = [1, 3, 5, 8, 10, 13, 17, 20, 22, 25, 27, 29]
            if tarfnameSplit[2] == "LP3":
                leechers = [1, 2, 3, 4, 5, 7, 11, 13, 17, 19, 23, 25, 26, 27, 28, 29]
            if tarfnameSplit[2] == "LP4":
                leechers = [1, 2, 3, 4, 5, 7, 8, 10, 11, 13, 17, 19, 20, 22, 23, 25, 26, 27, 28, 29]
            if tarfnameSplit[2] == "LP5":
                leechers = [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 13, 14, 16, 17, 19, 20, 21, 22, 23, 25, 26, 27, 28, 29]

        leechers = list(map(int,leechers))
        
        # create output folder for all analysis files produced by this script
        outputPath = "./extractedRates/"+tarfname+"_out/"
        outputFile = "./extractedRates/"+tarfname+"_out"

        if os.path.exists((outputFile+".zip")):
            print((outputPath+".zip" + " already processed"))
            continue
        
        if not os.path.exists(outputPath):
            os.makedirs((outputPath), exist_ok=True)
        else:
            print((outputFile+".zip" + " is being processed"))
            continue
        
        logging.basicConfig(level=logging.DEBUG, filename=(outputPath+"error.log"))
        somethingWasProcessed=True
        try:
            tarf = open_tar_file(tar_file)
            if tarf != False:
                
                filelist = tarf.getnames()
                      
                for filename in filelist:
                    split_filename = re.split(r'[/]', filename)
                    if((len(split_filename) == 4) and (split_filename[2] == "stats")):
                        foldername = split_filename[3]
                        break
                        
                lastDate = calc_ul_start_from_stats_foldername(foldername)
                print("lastDate: "+str(lastDate))
                
                # generate Sent/ReceivedPiece*.log's using regex's for piece transmissions from az.log's of nodes
                # no creation of Received*.log for seed
                
                #SendLogs(seeder,leechers)

            grepLogs(seed,leechers,tarf,tarfname)
            receivedRegex = "\s0\snet(\s)+Received\s\[BT\_PIECE\sdata\sfor\spiece\s\#|\]\smessage(\s\.)*(\s)?TorrentDLM:\s\'testfile050mb\';\sPeer:\s([R,L]:\s|\/)192\.168\.123\.|:[\s]*(\d){2,5}(\(UDP\))*\s\[Azureus 4.3.1.4\]"
            sentRegex = "\s0\snet(\s)+Sent\s\[BT\_PIECE\sdata\sfor\spiece\s\#|\]\smessage(\s\.)*(\s)?TorrentDLM:\s\'testfile050mb\';\sPeer:\s([R,L]:\s|\/)192\.168\.123\.|:[\s]*(\d){2,5}(\(UDP\))*\s\[Azureus 4.3.1.4\]"
            
            tc = azureusTimeConvert(lastDate)
            allData = pd.DataFrame(columns=["endTs","piece","fromClient","beginTs","toClient"])
            
            files = glob.glob(outputPath+"ReceivedPiece*.log")
            
            numberPattern = re.compile(r'\d+')
            allLines = int(0)
            
            # add initial seed to allNodes
            allNodes = [seed]
           
            print("Generating Piece Data")
            for file in files:
                
                #curReceiveNode = int(numberPattern.search(re.split(r'[\/]', file)[1]).group(0))
                curReceiveNode = int(numberPattern.search(os.path.split(file)[1]).group(0))
                allNodes.append(curReceiveNode)
                print("Receive Frame Data of Galileo " + str(curReceiveNode))
                currReceivingData = pd.read_csv(file,usecols=[0,7,14],header=None,parse_dates=False,sep=receivedRegex,engine='python')
                
                currReceivingData.columns = ["endTs","piece","fromClient"]
                # insert additional columns
                #currReceivingData.insert(3,"beginTs",None)
                #currReceivingData.insert(4,"toClient",None)

                # correct client id (ip-9 = id of client)
                currReceivingData.fromClient = currReceivingData.fromClient.subtract(9)

                # convert missing date (only clock in logs)
                tc.setLastTimestamp(lastDate)
                currReceivingData.endTs = currReceivingData.endTs.apply(tc.buggytimeconvert)
                currReceivingData.sort_values(['piece','endTs'],inplace=True)
               
                allLines = allLines + len(currReceivingData.index)
                
                for curSentNode in pd.unique(currReceivingData.fromClient):
                    
                    print("From Galileo" + str(curSentNode))
                    #print("curReceiveNode: " + str(curReceiveNode))
                    #print("curSentNode: " + str(curSentNode))
                    
                    sent = pd.read_csv((outputPath+'/SentPiece'+str(int(curSentNode))+'.log'),usecols=[0,7,14],header=None,parse_dates=False,sep=sentRegex,engine='python')
                    sent.columns = ["beginTs","piece","toClient"]
                    sent.toClient = sent.toClient.subtract(9)
                    tc.setLastTimestamp(lastDate)
                    sent.beginTs = sent.beginTs.apply(tc.buggytimeconvert)
                    sent.sort_values(['piece','beginTs'],inplace=True)
                    
                    receiverWindow = currReceivingData.loc[currReceivingData.fromClient == curSentNode].copy()#.set_index('piece')
                    senderWindow = sent[sent.toClient == curReceiveNode]#.set_index('piece')
                    
                    #print(receiverWindow)
                    #print(senderWindow)
            
                    #receiverWindow.loc[:,['beginTs','toClient']] = senderWindow.loc[:,['beginTs','toClient']]
                    receiverWindow = pd.merge(receiverWindow, senderWindow, how='inner', on=['piece'])
                    
                    #print(receiverWindow)
                    
                    allData = allData.append(receiverWindow.reset_index())
            
            with open(outputPath+"allData.txt", "w") as text_file:
                with pd.option_context('display.max_rows', None, 'display.max_columns', None, 'display.height', None, 'display.width', None):
                    print(allData.sort_values(['beginTs']), file=text_file)
                    print("\nTotal Lines: "+str(allLines), file=text_file)
            
            
            # for all node pairs: calculate sum of all subpiece sizes that were sent from "sendNode" to "receiveNode", re-sampled to 1 second windows
            #   -  determine number of lines making up a second -> take beginTime, increment line count as long as (endTime-beginTime) <= 1 second
            #   -  mark next beginTime as start of new second
            #   -  resulting datarate = #lines * subpieceSize  (in kbps)
            #   -  remember sending/receiveNode pair
            
            firstTimestamp = lastDate # reference time -> start of all 1 second windows
            startOfSecond = lastDate # 1 second window start timestamp (init with reference time)
            
            # count of 1 second windows (dataframe size, loop length to create sampledData)
            windowCount = int( math.ceil( (allData.endTs.max()-firstTimestamp).total_seconds() ) ) + 1
            print("windowCount: "+str(windowCount))
            
            #countPiecesTotal = 0.0 # total number of subpieces received by any "receiveNode"
            countPiecesPerSecond = 0.0 # number of subpieces per 1 second window for a node pair
            
            countPiecesStartingEndingWithinWindowTotal = 0.0
            countPiecesStartingBeforeWindowTotal = 0.0
            countPiecesEndingAfterWindowTotal = 0.0
            countPiecesStartingEndingOutsideWindowTotal = 0.0
            
            pieceSize = (16384*8)/1000.0  # 16kB BT subpiece size -> use kilobits
            
            # sum of all receiveNodes' cumulative receive rates
            sumRatesTotal = pd.DataFrame(0, index=np.arange(windowCount), columns=["dataRatekbps"])
            
            # color scheme for figures
            NUM_COLORS = len(allNodes)-1
            cm = plt.get_cmap('gist_rainbow')
            cNorm  = colors.Normalize(vmin=0, vmax=NUM_COLORS-1)
            scalarMap = mplcm.ScalarMappable(norm=cNorm, cmap=cm)
            
            # diagram for all cumulative receive rates and sumRatesTotal
            plt.figure(0)
            ax = plt.subplot(2,1,1)
            ax.set_color_cycle([scalarMap.to_rgba(i) for i in range(NUM_COLORS)])
            
            for receiveNode in allNodes:
            
                print("Receive Node: Gal"+str(receiveNode))
            
                # cumulative receive rate from all sendNodes for current receiveNode
                sumRates = pd.DataFrame(0, index=np.arange(windowCount), columns=["dataRatekbps"])
                
                # diagram for receiveNode's receive rates    
                plt.figure(receiveNode)
                ax = plt.subplot(2,1,1)
                ax.set_color_cycle([scalarMap.to_rgba(i) for i in range(NUM_COLORS)])
                
                dataReceiveNode = allData.loc[(allData["toClient"] == receiveNode)]
                if len(dataReceiveNode) <= 0:
                    continue
                
          	#	countPiecesTotal = 0.0 # reset total piece counter at start of new receiveNode
                
                for sendNode in allNodes: # receiveNode <- sendNode
            
                    print("Send Node: Gal"+str(sendNode))
            
                    sampledData = pd.DataFrame(columns=["receiveNode","sendNode","second","dataRatekbps"])
            
                    if sendNode == receiveNode:
                        continue
            
                    # extract only the rows from "allData" that belong to pieces sent from sendNode to receiveNode
                    dataNodePair = allData.loc[(allData["toClient"] == receiveNode) & (allData["fromClient"] == sendNode)]
                    # sort rows by timestamp
                    dataNodePair = dataNodePair.sort_values(['beginTs'])
            
                    # re-sample piece transmissions into 1 second intervals, beginning with "startOfSecond", sum up piece count per window and store rate
            
                    startOfSecond = lastDate # reset reference time for 1 second windows
            
                    countPiecesStartingEndingWithinWindowTotal = 0.0
                    countPiecesStartingBeforeWindowTotal = 0.0
                    countPiecesEndingAfterWindowTotal = 0.0
                    countPiecesStartingEndingOutsideWindowTotal = 0.0
            
            #        with open("Gal"+str(receiveNode)+"_Gal"+str(sendNode)+"_PieceCounts.txt", "w") as text_file:
            #            print("", file=text_file)
            
                    for i in range(1, windowCount+1):
            
                        countPiecesPerSecond = 0.0 # reset per second piece counter at start of new node pair window
            
            #            with open("Gal"+str(receiveNode)+"_Gal"+str(sendNode)+"_PieceCounts.txt", "a") as text_file:
            #                print("\nSecond "+str(i)+":\n", file=text_file)
            
                        # count all transmissions started and completed within current 1 second window
                        piecesStartingEndingWithinWindow = dataNodePair.loc[(dataNodePair["beginTs"] >= startOfSecond) & (dataNodePair["endTs"] <= (startOfSecond + datetime.timedelta(seconds=1)))]
                        countPiecesStartingEndingWithinWindow = len(piecesStartingEndingWithinWindow)
                        countPiecesPerSecond += countPiecesStartingEndingWithinWindow
            
            #            with open("Gal"+str(receiveNode)+"_Gal"+str(sendNode)+"_PieceCounts.txt", "a") as text_file:
            #                print("\tpiecesStartingEndingWithinWindow: "+str(countPiecesStartingEndingWithinWindow), file=text_file)
            
                        # get all transmissions started before but ranging into current window
                        piecesStartingBeforeWindow = dataNodePair.loc[(dataNodePair["beginTs"] < startOfSecond) & (dataNodePair["endTs"] >= startOfSecond) & (dataNodePair["endTs"] <= (startOfSecond + datetime.timedelta(seconds=1)))]
                        countPiecesStartingBeforeWindow = 0.0
                        # iterate over pieces and increment piece count according to the fraction of overall transmission time that is part of the current window
                        for index, row in piecesStartingBeforeWindow.iterrows():
                            countPiecesStartingBeforeWindow += ( row.endTs - startOfSecond ).total_seconds() / ( row.endTs - row.beginTs ).total_seconds()
                        
                        countPiecesPerSecond += countPiecesStartingBeforeWindow
                        
            #            with open("Gal"+str(receiveNode)+"_Gal"+str(sendNode)+"_PieceCounts.txt", "a") as text_file:
            #                print("\tpiecesStartingBeforeWindow: "+str(countPiecesStartingBeforeWindow), file=text_file)
                        
                        # get all transmissions started within current 1 second window but ranging into subsequent window
                        piecesEndingAfterWindow = dataNodePair.loc[(dataNodePair["beginTs"] >= startOfSecond) & (dataNodePair["beginTs"] <= (startOfSecond + datetime.timedelta(seconds=1))) & (dataNodePair["endTs"] > (startOfSecond + datetime.timedelta(seconds=1)))]
                        countPiecesEndingAfterWindow = 0.0
                        # iterate over pieces and increment piece count according to the fraction of overall transmission time that is part of the current window
                        for index, row in piecesEndingAfterWindow.iterrows():
                            countPiecesEndingAfterWindow += ( (startOfSecond + datetime.timedelta(seconds=1)) - row.beginTs ).total_seconds() / ( row.endTs - row.beginTs ).total_seconds()
            
                        countPiecesPerSecond += countPiecesEndingAfterWindow
            
            #            with open("Gal"+str(receiveNode)+"_Gal"+str(sendNode)+"_PieceCounts.txt", "a") as text_file:
            #                print("\tpiecesEndingAfterWindow: "+str(countPiecesEndingAfterWindow), file=text_file)
            
                        # get all transmissions starting before and ending after current 1 second window
                        piecesStartingEndingOutsideWindow = dataNodePair.loc[(dataNodePair["beginTs"] < startOfSecond) & (dataNodePair["endTs"] > (startOfSecond + datetime.timedelta(seconds=1)))]
                        countPiecesStartingEndingOutsideWindow = 0.0
                        # iterate over pieces and increment piece count according to the fraction of overall transmission time that is part of the current window
                        for index, row in piecesStartingEndingOutsideWindow.iterrows():
                            countPiecesStartingEndingOutsideWindow += 1.0 / ( row.endTs - row.beginTs ).total_seconds()
                        
                        countPiecesPerSecond += countPiecesStartingEndingOutsideWindow
            
            #            with open("Gal"+str(receiveNode)+"_Gal"+str(sendNode)+"_PieceCounts.txt", "a") as text_file:
            #                print("\tpiecesStartingEndingOutsideWindow: "+str(countPiecesStartingEndingOutsideWindow), file=text_file)
            
            #            countPiecesTotal += countPiecesPerSecond
                        
                        countPiecesStartingEndingWithinWindowTotal += countPiecesStartingEndingWithinWindow
                        countPiecesStartingBeforeWindowTotal += countPiecesStartingBeforeWindow
                        countPiecesEndingAfterWindowTotal += countPiecesEndingAfterWindow
                        countPiecesStartingEndingOutsideWindowTotal += countPiecesStartingEndingOutsideWindow
            
                        rowSampledData = pd.DataFrame([[receiveNode,sendNode,i,countPiecesPerSecond*pieceSize]], columns=["receiveNode","sendNode","second","dataRatekbps"])
                        sampledData = pd.concat([sampledData,rowSampledData])
                        
                        # step to next 1 second window
                        startOfSecond = startOfSecond + datetime.timedelta(seconds=1)
            
            #        with open("Gal"+str(receiveNode)+"_Gal"+str(sendNode)+"_PieceCounts.txt", "a") as text_file:
            #            print("\n", file=text_file)
            #            print("countPiecesStartingEndingWithinWindowTotal: "+str(countPiecesStartingEndingWithinWindowTotal), file=text_file)
            #            print("countPiecesStartingBeforeWindowTotal: "+str(countPiecesStartingBeforeWindowTotal), file=text_file)
            #            print("countPiecesEndingAfterWindowTotal: "+str(countPiecesEndingAfterWindowTotal), file=text_file)
            #            print("countPiecesStartingEndingOutsideWindowTotal: "+str(countPiecesStartingEndingOutsideWindowTotal), file=text_file)
                    
                    # output all seconds and rates for current pair receiveNode <- sendNode
                    sampledData.to_csv(outputPath+"Gal"+str(receiveNode)+"_Gal"+str(sendNode)+"_receiveRate.csv", sep=',', encoding='utf-8')
                    
                    # add receive rate from current sendNode to receiveNode's cumulative rate
                    sumRates = pd.DataFrame(sumRates.values + sampledData.iloc[:,[3]].values, sumRates.index, sumRates.columns)
                    
                    # plot graph for receive rate from current sendNode
                    ax.plot(sampledData["dataRatekbps"].values.tolist(), label='G'+str(sendNode))
                    
                    
                # update network-wide cumulative receive rate with receiveNode's cumulative receive rate
                sumRatesTotal = pd.DataFrame(sumRatesTotal.values + sumRates.values, sumRatesTotal.index, sumRatesTotal.columns)
                
                # save cumulative rate for receiveNode in separate csv
                sumRates.to_csv(outputPath+"Gal"+str(receiveNode)+"_receiveRateSum.csv", sep=',', encoding='utf-8')
                
                # configure sizing/legend for first subplot of figure for receiveNode
                lgd1 = plt.legend(loc=2, prop={'size':6}, bbox_to_anchor=(1.05,1))
                plt.tight_layout(pad=3)
                
                plt.title("Receive Rates Galileo "+str(receiveNode))
                plt.xlabel("Time [s]")
                plt.ylabel("Receive Rate [kbps]")
                
                # plot graph for receiveNode's cumulative receive rate
                ax = plt.subplot(2,1,2)
                ax.plot(sumRates["dataRatekbps"].values.tolist(), label="Sum")
                
                # configure sizing/legend for second subplot of figure for receiveNode
                lgd2 = plt.legend(loc=2, prop={'size':6}, bbox_to_anchor=(1.05,1))
                plt.tight_layout(pad=3)
                
                plt.title("Receive Rate Sum Galileo "+str(receiveNode))
                plt.xlabel("Time [s]")
                plt.ylabel("Receive Rate [kbps]")
                
                # save figure for receiveNode
                plt.savefig(outputPath+"Gal"+str(receiveNode)+"_receiveRate.svg", bbox_extra_artists=(lgd1,lgd2), bbox_inches='tight')

                plt.close(plt.figure(receiveNode))
                
                # plot receiveNode's cumulative receive rate also in first subplot of additional figure
                plt.figure(0)
                ax = plt.subplot(2,1,1)
                ax.plot(sumRates["dataRatekbps"].values.tolist(), label="SumG"+str(receiveNode))
            
            #    with open("Gal"+str(receiveNode)+"_PieceCountsTotal.txt", "w") as text_file:
            #        print("Total Piece Count: "+str(countPiecesTotal), file=text_file)
            
            
            # plot additional figure with all cumulative rates (per receiveNode and network-wide)
            plt.figure(0)
            # configure sizing/legend of first subplot of figure for network utilization
            plt.subplot(2,1,1)
            lgd1 = plt.legend(loc=2, prop={'size':6}, bbox_to_anchor=(1.05,1))
            plt.tight_layout(pad=3)
            plt.title("Receive Rate Sums")
            plt.xlabel("Time [s]")
            plt.ylabel("Receive Rate [kbps]")
            # plot network-wide cumulative receive rate in second subplot of additional figure
            ax = plt.subplot(2,1,2)
            ax.plot(sumRatesTotal["dataRatekbps"].values.tolist(), label="SumTotal")
            # configure sizing/legend of second subplot of figure for network utilization
            lgd2 = plt.legend(loc=2, prop={'size':6}, bbox_to_anchor=(1.05,1))
            plt.tight_layout(pad=3)
            plt.title("Total Receive Rate Sum")
            plt.xlabel("Time [s]")
            plt.ylabel("Receive Rate [kbps]")
            # save figure for network utilization
            plt.savefig(outputPath+"allReceiveRateSums.svg", bbox_extra_artists=(lgd1,lgd2), bbox_inches='tight')
            
            plt.close(plt.figure(0))
            
            # save network-wide cumulative receive rate in separate csv
            sumRatesTotal.to_csv(outputPath+"allReceiveRateSums.csv", sep=',', encoding='utf-8')
            
            # store output folder as zip archive, delete temporary data
            shutil.make_archive(outputFile, 'zip', outputPath)
            shutil.rmtree((outputPath), ignore_errors=True)           
                
        except SystemExit:
            sys.exit(0)
        except:
            logging.exception("Oops:")
        
