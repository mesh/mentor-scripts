#!/bin/bash

sudo apt-get install python3-pip python3-tk parallel
sudo pip3 install --upgrade pip
sudo pip3 install pandas matplotlib
