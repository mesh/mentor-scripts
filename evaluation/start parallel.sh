#!/bin/bash

scriptPath=$(pwd)

while true; do
	(ls -1 -d MESH_MCS3/MESH*/ && ls -1 -d MESH_MCS3/MESH*/ && ls -1 -d MESH_MCS3/MESH*/ && ls -1 -d MESH_MCS3/MESH*/ && echo ".") | parallel --joblog ./parallel_log_`hostname` "cd {} && python3 $scriptPath/extractRatesBT.py"
	sleep 10m
done
