#!/bin/bash

for i in ${SWARM[*]}
do

	echo "Copying to Galileo $i ..."

	#ssh root@"galileo$i" "mkdir -p /home/root/Azureus"

	scp -rp deploy/Azureus_Galileo/* root@"galileo$i":/home/root/Azureus/ &
	scp -rp deploy/Azureus_Galileo/Azureus_4.3.1.4_LANmod_linux_32.jar root@"galileo$i":/home/root/Azureus/Azureus_4.3.1.4_LANmod_linux_32.jar &

	#scp deploy/testfile050mb root@"galileo$i":/home/root/ &

	#scp -rp deploy/Azureus_Galileo/plugins/* root@"galileo$i":/home/root/Azureus/plugins &

done

