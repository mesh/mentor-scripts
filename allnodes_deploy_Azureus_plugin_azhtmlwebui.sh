#!/bin/bash

for i in ${SWARM[*]}
do

	echo "Creating folder on Galileo $i ..."

	ssh root@"galileo$i" "mkdir -p /home/root/Azureus/config/Default/_.azureus/plugins/azhtmlwebui" &

done

sleep 5

for i in ${SWARM[*]}
do

	echo "Copying to Galileo $i ..."

	scp deploy/Azureus_Galileo/config/Default/_.azureus/plugins/azhtmlwebui/* root@"galileo$i":/home/root/Azureus/config/Default/_.azureus/plugins/azhtmlwebui/ &
	
done

