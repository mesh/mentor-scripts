#!/bin/bash

#TRACKER_INITSEED=1
#LEECHERS=(3 5 13 15 17 25 27 29)
#ALLNODES=(1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29)


# indexed array (galileo number = idx) to store DL duration in seconds
durations=()

# initialize array
for i in $( seq 1 1 ${#ALLNODES[@]} ); do
	durations[$i]=0
done


# retrieve UL start time reference from initial seed (file 'ul_begin_*')

cd "Galileo$TRACKER_INITSEED"

beginFilename=$(ls -al ul_begin_* | awk '{print $9}')

beginHours=$(echo $beginFilename | awk -F '[.]' '{print $4}')
beginMinutes=$(echo $beginFilename | awk -F '[.]' '{print $5}')
beginSeconds=$(echo $beginFilename | awk -F '[.]' '{print $6}')

cd ..

for i in ${LEECHERS[*]}; do

		cd "Galileo$i"

		# retreive DL completion time reference from leechers (file 'dl_complete_*')

		completeFilename=$(ls -al dl_complete_* | awk '{print $9}')

		completeHours=$(echo $completeFilename | awk -F '[.]' '{print $4}')
		completeMinutes=$(echo $completeFilename | awk -F '[.]' '{print $5}')
		completeSeconds=$(echo $completeFilename | awk -F '[.]' '{print $6}')

		# calc time difference and store as DL duration

		if [ $completeHours -lt $beginHours ]; then

			# handle case where 'completeHours' is smaller than 'beginHours' (DL went over 12:59 am/pm)
			
			durationHours=0 # TODO: handle case where DL takes longer than 60 minutes (compare minutes)
			durationMinutes=$(($((3600-10#$beginMinutes*60))+((10#$completeMinutes*60))))
			durationSeconds=$(($((60-10#$beginSeconds))+((10#$completeSeconds))))

			duration=$((10#$durationHours+10#$durationMinutes+10#$durationSeconds))

		else
			# 'normal' timestamp case
			duration=$(($((10#$completeHours*3600+10#$completeMinutes*60+10#$completeSeconds))-$((10#$beginHours*3600+10#$beginMinutes*60+10#$beginSeconds))))
		fi

		durations[$i]=$duration

		cd ..
done


# calc min/max/avg duration from array -> ignore idx of initial seed / 0-durations

zero=0
count=0
sum=0
min=9999
max=0

for i in ${LEECHERS[*]}; do

	duration=${durations[i]}

	if [ $duration -gt $zero ]; then

		echo -e "DL duration Galileo$i:\t$duration" >> MinMaxAvgDLTime.txt

		count=$(($count+1))

		sum=$(($sum+$duration))

		if [ $duration -lt $min ]; then
			min=$duration
		fi

		if [ $duration -gt $max ]; then
			max=$duration
		fi
	fi
done

avg=$(($sum / $count))

# print min / max / avg

echo "" >> MinMaxAvgDLTime.txt
echo -e "DL duration min:\t$min" >> MinMaxAvgDLTime.txt
echo -e "DL duration max:\t$max" >> MinMaxAvgDLTime.txt
echo -e "DL duration avg:\t$avg" >> MinMaxAvgDLTime.txt
echo "" >> MinMaxAvgDLTime.txt

