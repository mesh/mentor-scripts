#!/bin/bash

for i in ${LEECHERS[*]}
do
		echo "Quitting Azureus on Galileo $i ..."

		ssh root@"galileo$i" "cd /home/root/Azureus && java -jar Azureus_4.3.1.4_LANmod_linux_32.jar --ui=console -c 'quit iamsure' >/dev/null 2>&1 & disown -h" &
done
