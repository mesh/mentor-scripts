#!/bin/bash

echo "Initialize Azureus Tracker / InitSeed on Galileo $TRACKER_INITSEED ..."

ssh root@"galileo$TRACKER_INITSEED" "cd /home/root/Azureus && ./start_Azureus_TrackerInitSeed.sh >/dev/null 2>&1 & disown -h" &
