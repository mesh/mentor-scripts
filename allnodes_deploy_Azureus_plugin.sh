#!/bin/bash

for i in ${SWARM[*]}
do

	echo "Copying to Galileo $i ..."

	scp deploy/Azureus_Galileo/config/Default/_.azureus/plugins/BassPlugin/BassPlugin.jar root@"galileo$i":/home/root/Azureus/config/Default/_.azureus/plugins/BassPlugin/BassPlugin.jar &
	
	#ssh root@"galileo$i" "rm -rf /home/root/Azureus/config/BassPlugin_confs" &

	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_vanilla									root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_vanilla_forbidOpt							root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_vanilla_forbidOpt_1ULSlot					root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_vanilla_allowUS							root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_vanilla_allowUS_forbidOpt					root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_vanilla_allowUS_forbidOpt_1ULSlot			root@"galileo$i":/home/root/Azureus/conf.txt &

	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM										root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_forbidOpt								root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_forbidOpt_1ULSlot						root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_allowUS								root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_allowUS_forbidOpt						root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_allowUS_forbidOpt_1ULSlot				root@"galileo$i":/home/root/Azureus/conf.txt &

	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_prefer1Hop								root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_prefer1Hop_forbidOpt					root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_prefer1Hop_forbidOpt_1ULSlot			root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_prefer1Hop_allowUS						root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_prefer1Hop_allowUS_forbidOpt			root@"galileo$i":/home/root/Azureus/conf.txt &
	#scp deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_prefer1Hop_allowUS_forbidOpt_1ULSlot	root@"galileo$i":/home/root/Azureus/conf.txt &

done

