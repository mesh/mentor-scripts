#!/bin/bash

#SWARM="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"

for i in ${SWARM[*]}
do
		echo "Quitting Azureus on Galileo $i ..."

		ssh root@"galileo$i" "cd /home/root/Azureus && java -jar Azureus_4.3.1.4_LANmod_linux_32.jar --ui=console -c 'quit iamsure' >/dev/null 2>&1 & disown -h" &
done
