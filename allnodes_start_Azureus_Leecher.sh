#!/bin/bash

for i in ${LEECHERS[*]}
do
		echo "Start Azureus Leecher on Galileo $i ..."

		ssh root@"galileo$i" "cd /home/root/Azureus && ./start_Azureus_Leecher.sh >/dev/null 2>&1 & disown -h" &
done
