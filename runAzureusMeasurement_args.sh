#!/bin/bash


# -> 5x5 grid allows for tracker/initSeed at symmetric center
# -> tracker/initSeed (corner, center)
# -> list of leechers (#interested, fix or random placement?)
# -> plugin config (18 combin.)
# -> X runs per setting
#
# -> #RUNS = #tracker_placements * #leecher_placements * #plugin_configs * X
#
# -> 5x5:	ETH: 1*5*6*X + MESH: 2*5*18*X					-> 2100 runs for X=10 (1050h if 30min per run -> 6,25  weeks ~ 1,5  months)
#			+ 60 for full 6x6 ETH + 360 for full 6x6 MESH	-> 2520 runs for X=10 (1260h if 30min per run -> 7,50  weeks ~ 2,0  months))
#
#			ETH: 1*5*6*X + MESH: 2*5*18*X					-> 1015 runs for X=5  ( 525h if 30min per run -> 3,125 weeks ~ 0,75 months)
#			+ 30 for full 6x6 ETH + 180 for full 6x6 MESH	-> 1260 runs for X=5  ( 630h if 30min per run -> 3,75  weeks ~ 1,0  months))


TECH_ETH="ETH"
TECH_MESH="MESH"


# TESTBED GRID:
#
# 31 32 33 34 35 36
# --------------
# 25 26 27 28 29|30
# 19 20 21 22 23|24
# 13 14 15 16 17|18
#  7  8  9 10 11|12
#  1  2  3  4  5| 6

ALLNODES_5x5=(1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29)
ALLNODES_6x6=(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36)

ALLNODES=( "${ALLNODES_5x5[*]}" ) # all nodes in test bed (max. swarm)


TRACKER_INITSEED_CORNER=1	# corner:	5x5:  1 (5 25 29)	/ 6x6:  1 (6 31 36)
TRACKER_INITSEED_CENTER=15	# center:	5x5: 15		   		/ 6x6: 15 16 21 22


# 5x5, TRACKER_INITSEED=1
# -----------------------

LEECHERS_TIS1_8=(3 5 13 15 17 25 27 29) # 8
# 25 -- 27 -- 29
# -- -- -- -- --
# 13 -- 15 -- 17
# -- -- -- -- --
#  1 --  3 --  5

LEECHERS_TIS1_12=(3 5 8 10 13 15 17 20 22 25 27 29) # 12
# 25 -- 27 -- 29
# -- 20 -- 22 --
# 13 -- 15 -- 17
# --  8 -- 10 --
#  1 --  3 --  5

LEECHERS_TIS1_16=(2 3 4 5 7 11 13 15 17 19 23 25 26 27 28 29) # 16
# 25 26 27 28 29
# 19 -- -- -- 23
# 13 -- 15 -- 17
#  7 -- -- -- 11
#  1  2  3  4  5

LEECHERS_TIS1_20=(2 3 4 5 7 8 10 11 13 15 17 19 20 22 23 25 26 27 28 29) # 20
# 25 26 27 28 29
# 19 20 -- 22 23
# 13 -- 15 -- 17
#  7  8 -- 10 11
#  1  2  3  4  5

LEECHERS_TIS1_24=(2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29) # 24
# 25 26 27 28 29
# 19 20 21 22 23
# 13 14 15 16 17
#  7  8  9 10 11
#  1  2  3  4  5


# 5x5, TRACKER_INITSEED=15
# ------------------------

LEECHERS_TIS15_8=(1 3 5 13 17 25 27 29) # 8
# 25 -- 27 -- 29
# -- -- -- -- --
# 13 -- 15 -- 17
# -- -- -- -- --
#  1 --  3 --  5

LEECHERS_TIS15_12=(1 3 5 8 10 13 17 20 22 25 27 29) # 12
# 25 -- 27 -- 29
# -- 20 -- 22 --
# 13 -- 15 -- 17
# --  8 -- 10 --
#  1 --  3 --  5

LEECHERS_TIS15_16=(1 2 3 4 5 7 11 13 17 19 23 25 26 27 28 29) # 16
# 25 26 27 28 29
# 19 -- -- -- 23
# 13 -- 15 -- 17
#  7 -- -- -- 11
#  1  2  3  4  5

LEECHERS_TIS15_20=(1 2 3 4 5 7 8 10 11 13 17 19 20 22 23 25 26 27 28 29) # 20
# 25 26 27 28 29
# 19 20 -- 22 23
# 13 -- 15 -- 17
#  7  8 -- 10 11
#  1  2  3  4  5

LEECHERS_TIS15_24=(1 2 3 4 5 7 8 9 10 11 13 14 16 17 19 20 21 22 23 25 26 27 28 29) # 24
# 25 26 27 28 29
# 19 20 21 22 23
# 13 14 15 16 17
#  7  8  9 10 11
#  1  2  3  4  5


#PLUGIN_CONF=1
#
#  1 vanilla
#  2 vanilla_forbidOpt
#  3 vanilla_forbidOpt_1ULSlot
#  4 vanilla_allowUS
#  5 vanilla_allowUS_forbidOpt
#  6 vanilla_allowUS_forbidOpt_1ULSlot
#
#  7 ALM
#  8 ALM_forbidOpt
#  9 ALM_forbidOpt_1ULSlot
# 10 ALM_allowUS
# 11 ALM_allowUS_forbidOpt
# 12 ALM_allowUS_forbidOpt_1ULSlot
#
# 13 ALM_prefer1Hop
# 14 ALM_prefer1Hop_forbidOpt
# 15 ALM_prefer1Hop_forbidOpt_1ULSlot
# 16 ALM_prefer1Hop_allowUS
# 17 ALM_prefer1Hop_allowUS_forbidOpt
# 18 ALM_prefer1Hop_allowUS_forbidOpt_1ULSlot


FILEPATH_SCRIPTS=/home/mret/Arbeitsfläche/btsync/mesh_test_bed_share/scripts_michael_Azureus_only_debian
FILEPATH_LOGS_STATS=/home/mret/Arbeitsfläche/azureus_log
#FILEPATH_LOGS_STATS=/home/mret/.gvfs/user\ auf\ mdnt3.amd.e-technik.uni-rostock.de/Mitarbeiter/temp/azureus_log

# ---------------------------------------------------------------------------------------------

# TODO read arguments and assign to variables

TECH="$1"
TRACKER_INITSEED_PLACEMENT="$2"
LEECHER_PLACEMENT="$3"
PLUGIN_CONF="$4"
RUN="$5"

# ---------------------------------------------------------------------------------------------


case "$TECH" in

"$TECH_ETH")

# ---------------------------------------------------------------------------------------------
# ETHERNET
# ---------------------------------------------------------------------------------------------

	TRACKER_INITSEED=$TRACKER_INITSEED_CORNER	# no variation for ETH

	case $LEECHER_PLACEMENT in

		1) #  8 leechers
		LEECHERS=( "${LEECHERS_TIS1_8[*]}" )
		;;
		2) # 12 leechers
		LEECHERS=( "${LEECHERS_TIS1_12[*]}" )
		;;
		3) # 16 leechers
		LEECHERS=( "${LEECHERS_TIS1_16[*]}" )
		;;
		4) # 20 leechers
		LEECHERS=( "${LEECHERS_TIS1_20[*]}" )
		;;
		5) # 24 leechers
		LEECHERS=( "${LEECHERS_TIS1_24[*]}" )
		;;
	esac

	SWARM=( $TRACKER_INITSEED "${LEECHERS[*]}" ) # current swarm


	# 0) deploy startscript / torrent file / plugin conf
	echo "0) deploy startscript / torrent file / plugin conf"
	source allnodes_deploy_Azureus_startScripts.sh
	source allnodes_deploy_Azureus_torrents.sh
	source allnodes_deploy_Azureus_plugin_conf.sh
	sleep 20

	echo "CURRENT RUN: $TECH.TIS$TRACKER_INITSEED.LP$LEECHER_PLACEMENT.PC$PLUGIN_CONF.R$RUN"

	# TODO: ssh reachability check on SWARM -> terminate on errors
	# ...
	source allnodes_restart_ptpd_forced.sh
	sleep 5


	# 1) initialize tracker/initial seed
	echo "1) initialize tracker/initial seed"
	source allnodes_init_Azureus_TrackerInitSeed.sh


	# 2) wait for tracker/initial seed to become ready			# TODO: maximum retry count for check script
	echo "2) wait for tracker/initial seed to become ready"
	while true; do

		sleep 20

		source allnodes_check_Azureus_TrackerInitSeed_rdy.sh

		if [[ $tracker_initSeed_rdy == 1 ]]; then
			echo "ready!"
			break
		else
			echo "not ready yet ..."
		fi
	done


	# 3) start leechers
	echo "3) start leechers"
	source allnodes_start_Azureus_Leecher.sh


	# 4) wait for leechers to become ready					# TODO: maximum retry count for check script
	echo "4) wait for leechers to become ready"
	sleep 100
	while true; do

		sleep 20

		source allnodes_check_Azureus_Leecher_rdy.sh

		if [[ $leechers_rdy == 1 ]]; then
			echo "all ready!"
			break
		else
			echo "not all ready yet ..."
		fi
	done


	# 5) tell initial seed to start seeding
	echo "5) tell initial seed to start seeding"
	source allnodes_activate_Azureus_TrackerInitSeed.sh


	# 6) wait for leechers to finish DL
	echo "6) wait for leechers to finish DL"
	sleep 100
	while true; do

		sleep 20

		source allnodes_check_Azureus_Leecher_finished.sh		# TODO: maximum retry count for check script
		
		if [[ $leechers_finished == 1 ]]; then
			echo "all finished!"
			break
		else
			echo "not all finished yet ..."
		fi
	done


	# 7) stop all Azureus instances
	echo "7) stop all Azureus instances"
	source allnodes_stop_Azureus.sh


	# 8) wait until all Azureus instances are stopped			# TODO: maximum retry count for check script
	echo "8) wait until all Azureus instances are stopped"
	while true; do

		sleep 20

		source allnodes_check_Azureus_stopped.sh

		if [[ $swarm_stopped == 1 ]]; then
			echo "all stopped!"
			break
		else
			echo "not all stopped yet ..."
		fi
	done


	# 9) get Logs/Stats from all nodes
	echo "9) get Logs/Stats from all nodes"
	source allnodes_get_Azureus_LogsStats.sh

;;


"$TECH_MESH")

# ---------------------------------------------------------------------------------------------
# MESH
# ---------------------------------------------------------------------------------------------

	case $TRACKER_INITSEED_PLACEMENT in

		1) # Tracker/InitSeed in grid corner
		TRACKER_INITSEED=$TRACKER_INITSEED_CORNER

		case $LEECHER_PLACEMENT in

			1) #  8 leechers
			LEECHERS=( "${LEECHERS_TIS1_8[*]}" )
			;;
			2) # 12 leechers
			LEECHERS=( "${LEECHERS_TIS1_12[*]}" )
			;;
			3) # 16 leechers
			LEECHERS=( "${LEECHERS_TIS1_16[*]}" )
			;;
			4) # 20 leechers
			LEECHERS=( "${LEECHERS_TIS1_20[*]}" )
			;;
			5) # 24 leechers
			LEECHERS=( "${LEECHERS_TIS1_24[*]}" )
			;;
		esac

		;;

		2) # Tracker/InitSeed in grid center
		TRACKER_INITSEED=$TRACKER_INITSEED_CENTER

		case $LEECHER_PLACEMENT in

			1) #  8 leechers
			LEECHERS=( "${LEECHERS_TIS15_8[*]}" )
			;;
			2) # 12 leechers
			LEECHERS=( "${LEECHERS_TIS15_12[*]}" )
			;;
			3) # 16 leechers
			LEECHERS=( "${LEECHERS_TIS15_16[*]}" )
			;;
			4) # 20 leechers
			LEECHERS=( "${LEECHERS_TIS15_20[*]}" )
			;;
			5) # 24 leechers
			LEECHERS=( "${LEECHERS_TIS15_24[*]}" )
			;;
		esac

		;;
	esac

	SWARM=( $TRACKER_INITSEED "${LEECHERS[*]}" ) # current swarm


	# 0) deploy startscript / torrent file / plugin conf
	echo "0) deploy startscript / torrent file / plugin conf"
	source allnodes_deploy_Azureus_startScripts.sh
	source allnodes_deploy_Azureus_torrents.sh
	source allnodes_deploy_Azureus_plugin_conf.sh
	sleep 20

	echo "CURRENT RUN: $TECH.TIS$TRACKER_INITSEED.LP$LEECHER_PLACEMENT.PC$PLUGIN_CONF.R$RUN"

	# TODO: ssh reachability check on SWARM -> terminate on errors
	# ...


	# 1) initialize tracker/initial seed
	echo "1) initialize tracker/initial seed"
	source allnodes_init_Azureus_TrackerInitSeed.sh


	# 2) wait for tracker/initial seed to become ready			# TODO: maximum retry count for check script
	echo "2) wait for tracker/initial seed to become ready"
	while true; do

		sleep 20

		source allnodes_check_Azureus_TrackerInitSeed_rdy.sh

		if [[ $tracker_initSeed_rdy == 1 ]]; then
			echo "ready!"
			break
		else
			echo "not ready yet ..."
		fi
	done


	# 3) start leechers
	echo "3) start leechers"
	source allnodes_start_Azureus_Leecher.sh


	# 4) wait for leechers to become ready					# TODO: maximum retry count for check script
	echo "4) wait for leechers to become ready"
	sleep 100

	# TODO
	tryCount=0

	while true; do

		sleep 20

		source allnodes_check_Azureus_Leecher_rdy.sh

		if [[ $leechers_rdy == 1 ]]; then
			echo "all ready!"
			break
		else
			echo "not all ready yet ..."
			
			# TODO
			tryCount=$(( $tryCount + 1 ))
			
			# TODO
			if (( "$tryCount" > 5 )); then
				echo "reached max. try count, restarting ..."
				tryCount=0
				source allnodes_stop_Azureus_Leecher.sh
				while true; do
					sleep 20
					source allnodes_check_Azureus_Leecher_stopped.sh
					if [[ $leechers_stopped == 1 ]]; then
						break
					fi
				done
				source allnodes_start_Azureus_Leecher.sh
				sleep 100
			fi

		fi

	done


	# 5) tell initial seed to start seeding
	echo "5) tell initial seed to start seeding"
	source allnodes_activate_Azureus_TrackerInitSeed.sh


	# 6) wait for leechers to finish DL					# TODO: maximum retry count for check script
	echo "6) wait for leechers to finish DL"
	sleep 100
	while true; do

		sleep 20

		source allnodes_check_Azureus_Leecher_finished.sh

		if [[ $leechers_finished == 1 ]]; then
			echo "all finished!"
			break
		else
			echo "not all finished yet ..."
		fi
	done


	# 7) stop all Azureus instances
	echo "7) stop all Azureus instances"
	source allnodes_stop_Azureus.sh


	# 8) wait until all Azureus instances are stopped			# TODO: maximum retry count for check script
	echo "8) wait until all Azureus instances are stopped"
	while true; do

		sleep 20

		source allnodes_check_Azureus_stopped.sh

		if [[ $swarm_stopped == 1 ]]; then
			echo "all stopped!"
			break
		else
			echo "not all stopped yet ..."
		fi
	done


	# 9) get Logs/Stats from all nodes
	echo "9) get Logs/Stats from all nodes"
	source allnodes_get_Azureus_LogsStats.sh

;;

esac

