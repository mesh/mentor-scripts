#!/bin/bash

file="/home/root/Azureus/ul_ready_*"

tracker_initSeed_rdy=1

echo "Check Azureus Tracker / InitSeed Rdy on Galileo $TRACKER_INITSEED ..."

if ssh root@"galileo$TRACKER_INITSEED" test -e $file;
	then
		echo "Galileo $TRACKER_INITSEED rdy!"
		tracker_initSeed_rdy=$tracker_initSeed_rdy
	else
		echo "Galileo $TRACKER_INITSEED not rdy!"
		tracker_initSeed_rdy=0
fi
