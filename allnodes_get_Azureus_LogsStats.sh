#!/bin/bash

cd "$FILEPATH_LOGS_STATS"

DIR_NAME="$TECH.TIS$TRACKER_INITSEED.LP$LEECHER_PLACEMENT.PC$PLUGIN_CONF.R$RUN"
mkdir "$DIR_NAME"

cp "$FILEPATH_SCRIPTS/calcMinMaxAvgDLTime.sh" "$DIR_NAME"

cd "$DIR_NAME"

for i in ${SWARM[*]}
do

		echo "Getting Azureus Logs & Stats from Galileo $i ..."

		mkdir "Galileo$i"

		rsync -tr --inplace root@"galileo$i":/home/root/Azureus/conf.txt "Galileo$i"/conf.txt &
		rsync -tr --inplace root@"galileo$i":/home/root/Azureus/ul_* "Galileo$i"/ &
		rsync -tr --inplace root@"galileo$i":/home/root/Azureus/dl_* "Galileo$i"/ &
		rsync -tr --inplace root@"galileo$i":/home/root/Azureus/az.* "Galileo$i"/ &
		rsync -tr --inplace root@"galileo$i":/home/root/Azureus/Azureus_Stats.xml "Galileo$i"/Azureus_Stats.xml &
		rsync -tr --inplace root@"galileo$i":/home/root/.azureus/stats "Galileo$i"/ &
		rsync -tr --inplace root@"galileo$i":/home/root/.azureus/logs "Galileo$i"/ &

done

sleep 30


source calcMinMaxAvgDLTime.sh

cd ..

export GZIP=-9
tar -zcf "$DIR_NAME.tar.gz" "$DIR_NAME"
rm -r "$DIR_NAME"

cd "$FILEPATH_SCRIPTS"

