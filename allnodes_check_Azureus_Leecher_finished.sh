#!/bin/bash

file="/home/root/Azureus/dl_complete_*"

leechers_finished=1

LIST=""

for i in ${LEECHERS[*]}
do
		echo "Check Azureus Leecher Finished on Galileo $i ..."
		LIST="${LIST}root@galileo${i},"
done

output=$(parallel --gnu --nonall -S $LIST "test -e $file && echo \"\$(iam)-0\" || echo \"\$(iam)-1\"")

for i in $output ; do
	if [[ "$i" =~ "Galileo"[0-9]{1,2}"-0" ]] ; then
		echo "${i::-2} finished"
		leechers_finished=$leechers_finished
	else
		echo "${i::-2} not finished"
		leechers_finished=0
	fi
done
