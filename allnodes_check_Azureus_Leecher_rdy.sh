#!/bin/bash

file="/home/root/Azureus/dl_begin_*"

leechers_rdy=1

LIST=""

for i in ${LEECHERS[*]}
do
		echo "Check Azureus Leecher Ready on Galileo $i ..."
		LIST="${LIST}root@galileo${i},"
done

output=$(parallel --gnu --nonall -S $LIST "test -e $file && echo \"\$(iam)-0\" || echo \"\$(iam)-1\"")

for i in $output ; do
	if [[ "$i" =~ "Galileo"[0-9]{1,2}"-0" ]] ; then
		echo "${i::-2} ready"
		leechers_rdy=$leechers_rdy
	else
		echo "${i::-2} not ready"
		leechers_rdy=0
	fi
done
