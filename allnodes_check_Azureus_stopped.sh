#!/bin/bash

#SWARM="1 2 3 4 5 7 8 9 10 11 13 14 15 16 17 19 20 21 22 23 25 26 27 28 29"

swarm_stopped=1

LIST=""

for i in ${SWARM[*]}
do
		echo "Check Azureus Leecher Stopped on Galileo $i ..."
		LIST="${LIST}root@galileo${i},"
done

output=$(parallel --gnu --nonall -S $LIST "test -z \$(pidof java) && echo \"\$(iam)-0\" || echo \"\$(iam)-1\"")

for i in $output ; do
	if [[ "$i" =~ "Galileo"[0-9]{1,2}"-0" ]] ; then
		echo "Azureus stopped on Galileo ${i::-2} !"
		swarm_stopped=$swarm_stopped
	else
		echo "Azureus not stopped on ${i::-2} !"
		swarm_stopped=0
	fi
done
