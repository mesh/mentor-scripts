#!/bin/bash

# iterate over all *.tar.gz archives
# (find . -name "*.tar.gz")
#
# from each archive extract file "MinMaxAvgDLTime.txt" and parse it
# (tar xvf /path/to/archive path/to/file)
#
# get the following lines to write into CSV
# include test case prefix (archive name without ending, e.g. "MESH.TIS1.LP1.PC1.R1"):
#	DL duration min:	xxxx
#	DL duration max:	xxxx
#	DL duration avg:	xxxx
#
# resulting output in CSV:
#
# Testcase,DurMin,DurMax,DurAvg
# ArchiveName1,xxxx,xxxx,xxxx
# ArchiveName2,xxxx,xxxx,xxxx
# ...

rm MinMaxAvgDLTime_all.csv
touch MinMaxAvgDLTime_all.csv
echo "Testcase,DurMin,DurMax,DurAvg" >> MinMaxAvgDLTime_all.csv

FILES=$(find . -maxdepth 1 -name "*.tar.gz")

for FILEPATH in $FILES; do

	#echo $FILEPATH
	FILENAME=$(echo $(basename $FILEPATH .tar.gz))
	#echo $FILENAME

	tar xvf $FILEPATH $FILENAME/MinMaxAvgDLTime.txt

	DURMIN=$(cat $FILENAME/MinMaxAvgDLTime.txt | grep 'DL duration min' | sed 's/[^0-9]//g')
	DURMAX=$(cat $FILENAME/MinMaxAvgDLTime.txt | grep 'DL duration max' | sed 's/[^0-9]//g')
	DURAVG=$(cat $FILENAME/MinMaxAvgDLTime.txt | grep 'DL duration avg' | sed 's/[^0-9]//g')
	#echo $DURMIN
	#echo $DURMAX
	#echo $DURAVG

	echo "$FILENAME,$DURMIN,$DURMAX,$DURAVG" >> MinMaxAvgDLTime_all.csv

	rm -r $FILENAME
done

