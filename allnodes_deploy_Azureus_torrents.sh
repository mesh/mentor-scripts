#!/bin/bash

for i in ${SWARM[*]}
do

	echo "Copying to Galileo $i ..."

	case "$TECH" in

		"$TECH_ETH")
		$(rsync -a deploy/Azureus_torrents/eth_public/testfile050mb_PieceSize0032kB_TGal$TRACKER_INITSEED.torrent root@"galileo$i":/home/root/Azureus/testfile050mb_PieceSize0032kB.torrent) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;

		"$TECH_MESH")
		$(rsync -a deploy/Azureus_torrents/mesh_public/testfile050mb_PieceSize0032kB_TGal$TRACKER_INITSEED.torrent root@"galileo$i":/home/root/Azureus/testfile050mb_PieceSize0032kB.torrent) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;

	esac

done
