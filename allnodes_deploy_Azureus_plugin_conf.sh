#!/bin/bash

for i in ${SWARM[*]}
do

	echo "Copying to Galileo $i ..."

	case $PLUGIN_CONF in

		1)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_vanilla									root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		2)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_vanilla_forbidOpt						root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		3)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_vanilla_forbidOpt_1ULSlot				root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		4)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_vanilla_allowUS							root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		5)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_vanilla_allowUS_forbidOpt				root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		6)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_vanilla_allowUS_forbidOpt_1ULSlot		root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		7)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM										root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		8)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_forbidOpt							root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		9)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_forbidOpt_1ULSlot					root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		10)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_allowUS								root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		11)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_allowUS_forbidOpt					root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		12)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_allowUS_forbidOpt_1ULSlot			root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		13)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_prefer1Hop							root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		14)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_prefer1Hop_forbidOpt					root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		15)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_prefer1Hop_forbidOpt_1ULSlot			root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		16)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_prefer1Hop_allowUS					root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		17)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_prefer1Hop_allowUS_forbidOpt			root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;
		18)
		$(rsync -a deploy/Azureus_Galileo/config/BassPlugin_confs/conf.txt_ALM_prefer1Hop_allowUS_forbidOpt_1ULSlot	root@"galileo$i":/home/root/Azureus/conf.txt) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		;;

	esac

done

