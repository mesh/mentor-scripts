#!/bin/bash

i=10
END=45

while [ $i -le $END ]; do
	
	nr=$(($i-9))

	sed -i 's/cp \/home\/root\/Azureus\/testfile050mb_PieceSize0032kB_TGal1.torrent \/home\/root\/Azureus\/testfile050mb_PieceSize0032kB.torrent.*$/cp \/home\/root\/Azureus\/testfile050mb_PieceSize0032kB_TGal'$nr'.torrent \/home\/root\/Azureus\/testfile050mb_PieceSize0032kB.torrent/' start_Azureus_TrackerInitSeed_Gal"$nr".sh

	sed -i 's/set \\"Bind IP\\" 192.168.123.10\\n.*$/set \\"Bind IP\\" 192.168.123.'$i'\\n/' start_Azureus_TrackerInitSeed_Gal"$nr".sh

	sed -i 's/set \\"Tracker IP\\" 192.168.123.10\\n.*$/set \\"Tracker IP\\" 192.168.123.'$i'\\n/' start_Azureus_TrackerInitSeed_Gal"$nr".sh

	i=$(($i+1))

done
