#!/bin/bash

i=10
END=45

while [ $i -le $END ]; do
	
	nr=$(($i-9))

	cp start_Azureus_TrackerInitSeed.sh "start_Azureus_TrackerInitSeed_Gal$nr.sh"

	i=$(($i+1))

done
