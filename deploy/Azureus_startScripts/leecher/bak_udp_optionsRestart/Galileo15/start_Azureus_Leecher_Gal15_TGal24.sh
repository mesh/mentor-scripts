#!/bin/bash

#set -x

echo 'Starting Azureus ...'


rm /home/root/Azureus/az.*
rm /home/root/Azureus/Azureus_Stats.xml
rm /home/root/Azureus/dl_*
rm /home/root/Azureus/ul_*
rm -rf /home/root/.azureus
mkdir /home/root/.azureus
cp -Rf /home/root/Azureus/config/Default/_.azureus/* /home/root/.azureus/

rm /home/root/Azureus/testfile050mb_PieceSize0032kB.torrent
cp /home/root/Azureus/testfile050mb_PieceSize0032kB_TGal24.torrent /home/root/Azureus/testfile050mb_PieceSize0032kB.torrent


# Leecher

rm /tmp/testfile050mb

echo 3 > /proc/sys/vm/drop_caches && free

/usr/java/jre1.8.0_11/bin/java -Xmx32m -Xss256k -XX:MaxMetaspaceSize=32m -XX:MaxDirectMemorySize=32m -jar /home/root/Azureus/Azureus_4.3.1.4_LANmod_linux_32.jar --ui=console -c $'

set \"Bind IP\" 192.168.123.24\n
set \"Enforce Bind IP\" 1\n

set \"AutoSpeed Available\" 0\n

set \"Auto Adjust Transfer Defaults\" 0\n
set \"Auto Upload Speed Enabled\" 0\n
set \"Auto Upload Speed Seeding Enabled\" 0\n

set Core_iMaxUploadSpeed 0\n
set Core_iMaxUploads 5\n
set Core_iMaxPeerConnectionsPerTorrent 50\n
set Core_iMaxPeerConnectionsTotal 50\n
set Max.Peer.Connections.Per.Torrent.When.Seeding 50\n

set \"Disconnect Seed\" 0\n

set enable.seedingonly.maxuploads 0\n

set \"Ip Filter Enabled\" 0\n
set \"Ip Filter Banning Persistent\" 0\n
set \"Ip Filter Enable Banning\" 0\n

set \"LAN Speed Enabled\" 0\n

set Logger.Enabled 1\n
set \"Logging Dir\" /home/root/Azureus\n
set \"Logging Enable\" 1\n
set \"Logging Enable UDP Transport\" 1\n
set \"Logging Max Size\" 500\n

set \"Max Download Speed KBs\" 0\n
set \"Max LAN Download Speed KBs\" 0\n
set \"Max LAN Upload Speed KBs\" 0\n
set \"Max Upload Speed KBs\" 0\n
set \"Max Uploads\" 5\n
set \"Max Uploads Seeding\" 5\n

set \"Network Selection Default.Public\" 1\n

set network.max.simultaneous.connect.attempts 50\n

set \"On Downloading Complete Do\" Nothing\n
set \"On Seeding Complete Do\" Nothing\n

set peercontrol.prefer.udp 1\n
set peercontrol.udp.fallback.connect.drop 1\n
set peercontrol.udp.fallback.connect.fail 1\n
set peercontrol.udp.probe.enable 1\n

set peermanager.schedule.time 100\n

set \"Peer Source Selection Default.Incoming\" 1\n
set \"Peer Source Selection Default.Plugin\" 1\n
set \"Peer Source Selection Default.Tracker\" 1\n
set \"Peer Source Selection Default.DHT\" 0\n
set \"Peer Source Selection Default.PeerExchange\" 0\n

set Plugin.BassPlugin.Bass.enable.stats 1\n
set Plugin.BassPlugin.Bass.main_settings.stats_dir /home/root/.azureus/stats\n
set Plugin.DHT.dht.enabled 0\n
set Plugin.DHT.dht.reachable.0 0\n
set Plugin.DHT.plugin.info 0\n
set Plugin.PluginUpdate.enable.update 0\n
set Plugin.UPnP.upnp.enable 0\n

set Proxy.Check.On.Start 0\n

set \"Server Enable UDP\" 1\n
set Server.shared.port 1\n

set \"Sharing Permit DHT\" 0\n
set \"Sharing Protocol\" UDP\n
set \"Sharing Torrent Private"\ 1\n

set StartStopManager_bIgnoreRatioPeers 1\n
set StartStopManager_iIgnoreRatioPeersSeedStart 1\n
set StartStopManager_iIgnoreSeedCount 1\n
set StartStopManager_iIgnoreShareRatioSeedStart 1\n
set StartStopManager_iMinSeedingTime 2147483647\n
set StartStopManager_iMinSpeedForActiveDL 0\n
set StartStopManager_iMinSpeedForActiveSeeding 0\n
set StartStopManager_iRankType 0\n

set \"Stats Dir\" /home/root/Azureus\n
set \"Stats Enable\" 1\n
set \"Stats Export File Details\" 1\n
set \"Stats Export Peer Details\" 1\n
set \"Stats File\" Azureus_Stats.xml\n
set \"Stats Period\" 60\n

set \"Tracker Client Connect Timeout\" 0\n
set \"Tracker Client Read Timeout\" 0\n
set \"Tracker Client Min Announce Interval\" 0\n
set \"Tracker Client Numwant Limit\" 100\n

set UDP.Listen.Port.Enable 1\n
set UDP.NonData.Listen.Port.Same 1\n

set update.autodownload 0\n
set update.opendialog 0\n
set update.periodic 0\n
set update.start 0\n

set \"Use Request Limiting\" 0\n
set \"Use Request Limiting Priorities\" 0\n

quit iamsure\n
'

rm /home/root/Azureus/az.*
echo 3 > /proc/sys/vm/drop_caches && free

/usr/java/jre1.8.0_11/bin/java -Xmx32m -Xss256k -XX:MaxMetaspaceSize=32m -XX:MaxDirectMemorySize=32m -jar /home/root/Azureus/Azureus_4.3.1.4_LANmod_linux_32.jar --ui=console -c $'

show torrents\n
add /home/root/Azureus/testfile050mb_PieceSize0032kB.torrent -o /tmp/\n
show torrents\n
start all\n
show torrents\n
'

