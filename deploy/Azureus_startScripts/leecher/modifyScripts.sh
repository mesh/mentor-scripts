#!/bin/bash

i=10
j=10
END=45

while [ $i -le $END ]; do
	
	nr1=$(($i-9))

	cd Galileo"$nr1"

	j=10
	while [ $j -le $END ]; do
	
		nr2=$(($j-9))

		if [ $nr1 != $nr2 ]; then

			sed -i 's/cp \/home\/root\/Azureus\/testfile050mb_PieceSize0032kB_TGal1.torrent \/home\/root\/Azureus\/testfile050mb_PieceSize0032kB.torrent.*$/cp \/home\/root\/Azureus\/testfile050mb_PieceSize0032kB_TGal'$nr2'.torrent \/home\/root\/Azureus\/testfile050mb_PieceSize0032kB.torrent/' start_Azureus_Leecher_Gal"$nr1"_TGal"$nr2".sh
			
			sed -i 's/set \\"Bind IP\\" 192.168.123.10\\n.*$/set \\"Bind IP\\" 192.168.123.'$i'\\n/' start_Azureus_Leecher_Gal"$nr1"_TGal"$nr2".sh

		fi

		j=$(($j+1))

	done

	cd ..

	i=$(($i+1))

done

