#!/bin/bash

i=10
j=10
END=45

while [ $i -le $END ]; do
	
	nr1=$(($i-9))

	j=10
	while [ $j -le $END ]; do
	
		nr2=$(($j-9))

		if [ $nr1 != $nr2 ]; then
			cp start_Azureus_Leecher.sh start_Azureus_Leecher_Gal"$nr1"_TGal"$nr2".sh
		fi

		j=$(($j+1))

	done

	mkdir Galileo"$nr1"
	mv start_Azureus_Leecher_Gal"$nr1"_* Galileo"$nr1"/

	i=$(($i+1))

done

