#!/bin/bash

#set -x

echo 'Starting Azureus ...'


rm /home/root/Azureus/az.log
rm /home/root/Azureus/az.log.bak
rm /home/root/Azureus/Azureus_Stats.xml
rm /home/root/Azureus/dl_*
rm /home/root/Azureus/ul_*
rm -rf /home/root/.azureus
mkdir /home/root/.azureus
cp -Rf /home/root/Azureus/config/Default/_.azureus/* /home/root/.azureus/

rm /home/root/Azureus/testfile050mb_PieceSize0032kB.torrent
cp /home/root/Azureus/testfile050mb_PieceSize0032kB_TGal36.torrent /home/root/Azureus/testfile050mb_PieceSize0032kB.torrent


# Leecher

rm /tmp/testfile050mb

echo 3 > /proc/sys/vm/drop_caches && free

/usr/java/jre1.8.0_11/bin/java -Xmx32m -Xss256k -XX:MaxMetaspaceSize=32m -XX:MaxDirectMemorySize=32m -Djava.net.preferIPv4Stack=true -jar /home/root/Azureus/Azureus_4.3.1.4_LANmod_linux_32.jar --ui=console -c $'

set \"Bind IP\" 192.168.123.17\n

set \"Auto Upload Speed Enabled\" 0\n
set \"Auto Upload Speed Seeding Enabled\" 0\n

set Core_iMaxPeerConnectionsPerTorrent 50\n
set Core_iMaxPeerConnectionsTotal 50\n
set Max.Peer.Connections.Per.Torrent.When.Seeding 50\n

set \"Disconnect Seed\" 0\n

set \"LAN Speed Enabled\" 0\n

set Logger.Enabled 1\n
set \"Logging Dir\" /home/root/Azureus\n
set \"Logging Enable\" 1\n
set \"Logging Max Size\" 50\n

set \"Max Download Speed KBs\" 0\n
set \"Max LAN Download Speed KBs\" 0\n
set \"Max LAN Upload Speed KBs\" 0\n
set \"Max Upload Speed KBs\" 0\n
set \"Max Uploads\" 4\n
set \"Max Uploads Seeding\" 4\n

set \"Peer Source Selection Default.DHT\" 0\n
set \"Peer Source Selection Default.PeerExchange\" 0\n

set Plugin.BassPlugin.Bass.enable.stats 1\n
set Plugin.BassPlugin.Bass.main_settings.stats_dir /home/root/.azureus/stats\n
set Plugin.DHT.dht.enabled 0\n
set Plugin.DHT.dht.reachable.0 0\n
set Plugin.DHT.plugin.info 0\n
set Plugin.PluginUpdate.enable.update 0\n
set Plugin.UPnP.upnp.enable 0\n

set \"Sharing Permit DHT\" 0\n
set \"Sharing Protocol\" HTTP\n

set StartStopManager_bIgnoreRatioPeers 1\n
set StartStopManager_iIgnoreRatioPeersSeedStart 1\n
set StartStopManager_iIgnoreSeedCount 1\n
set StartStopManager_iIgnoreShareRatioSeedStart 1\n
set StartStopManager_iMinSeedingTime 600000\n
set StartStopManager_iMinSpeedForActiveDL 0\n
set StartStopManager_iMinSpeedForActiveSeeding 0\n

set \"Stats Dir\" /home/root/Azureus\n
set \"Stats Enable\" 1\n
set \"Stats Export File Details\" 1\n
set \"Stats Export Peer Details\" 1\n
set \"Stats File\" Azureus_Stats.xml\n
set \"Stats Period\" 60\n

set enable.seedingonly.maxuploads 1\n

show torrents\n
remove all\n
show torrents\n
add /home/root/Azureus/testfile050mb_PieceSize0032kB.torrent -o /tmp/\n
show torrents\n
start all\n
show torrents\n
'

