
#!/bin/bash

i=10
j=10
END=45

while [ $i -le $END ]; do
	
	nr1=$(($i-9))

	cd Galileo"$nr1"

	j=10
	while [ $j -le $END ]; do
	
		nr2=$(($j-9))

		if [ $nr1 != $nr2 ]; then
			
			sed -i '/^rm \/home\/root\/Azureus\/dl_\*$/a rm \/home\/root\/Azureus\/ul_*' start_Azureus_Leecher_Gal"$nr1"_TGal"$nr2".sh

		fi

		j=$(($j+1))
	done

	cd ..

	i=$(($i+1))
done

