#!/bin/bash

leechers_stopped=1

LIST=""

for i in ${LEECHERS[*]}
do
		echo "Check Azureus Leecher Stopped on Galileo $i ..."
		LIST="${LIST}root@galileo${i},"
done

output=$(parallel --gnu --nonall -S $LIST "test -z \$(pidof java) && echo \"\$(iam)-0\" || echo \"\$(iam)-1\"")

for i in $output ; do
	if [[ "$i" =~ "Galileo"[0-9]{1,2}"-0" ]] ; then
		echo "Azureus stopped on Galileo ${i::-2} !"
		leechers_stopped=$leechers_stopped
	else
		echo "Azureus not stopped on ${i::-2} !"
		leechers_stopped=0
	fi
done

