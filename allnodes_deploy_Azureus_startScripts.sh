#!/bin/bash

for i in ${SWARM[*]}
do

	echo "Copying to Galileo $i ..."

	case "$TECH" in

		"$TECH_ETH")

		# eth TCP
		$(rsync -a deploy/Azureus_startScripts/tracker_initSeed/start_Azureus_TrackerInitSeed.sh_eth_tcp root@"galileo$i":/home/root/Azureus/start_Azureus_TrackerInitSeed.sh) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		$(rsync -a deploy/Azureus_startScripts/leecher/start_Azureus_Leecher.sh_eth_tcp root@"galileo$i":/home/root/Azureus/start_Azureus_Leecher.sh) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &

		# eth UDP
		#$rsync -a deploy/Azureus_startScripts/tracker_initSeed/start_Azureus_TrackerInitSeed.sh_eth_udp root@"galileo$i":/home/root/Azureus/start_Azureus_TrackerInitSeed.sh &
		#$rsync -a deploy/Azureus_startScripts/leecher/start_Azureus_Leecher.sh_eth_udp root@"galileo$i":/home/root/Azureus/start_Azureus_Leecher.sh &

		;;

		"$TECH_MESH")

		# mesh TCP
		$(rsync -a deploy/Azureus_startScripts/tracker_initSeed/start_Azureus_TrackerInitSeed.sh_mesh_tcp root@"galileo$i":/home/root/Azureus/start_Azureus_TrackerInitSeed.sh) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &
		$(rsync -a deploy/Azureus_startScripts/leecher/start_Azureus_Leecher.sh_mesh_tcp root@"galileo$i":/home/root/Azureus/start_Azureus_Leecher.sh) && echo -e "Galileo${i}\tSuccess" || echo -e "Galileo${i}\tError!" &

		# mesh UDP
		#rsync -a deploy/Azureus_startScripts/tracker_initSeed/start_Azureus_TrackerInitSeed.sh_mesh_udp root@"galileo$i":/home/root/Azureus/start_Azureus_TrackerInitSeed.sh &
		#rsync -a deploy/Azureus_startScripts/leecher/start_Azureus_Leecher.sh_mesh_udp root@"galileo$i":/home/root/Azureus/start_Azureus_Leecher.sh &

		;;

	esac

done
