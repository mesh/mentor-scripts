#!/bin/bash

echo "Activating Azureus Tracker / InitSeed on Galileo $TRACKER_INITSEED ..."

#command=$'show torrents\nforcestart all\n' # problem: second command not executed!
#ssh root@"galileo$TRACKER_INITSEED" "cd /home/root/Azureus && java -jar Azureus_4.3.1.4_LANmod_linux_32.jar --ui=console -c \"$command\" >/dev/null 2>&1 & disown -h" &

curl --silent --request GET "http://galileo$TRACKER_INITSEED:6886/index.tmpl?d=s&fstart=6E988" > /dev/null # web UI request to forcestart torrent -> check for valid torrent hash!
