#!/bin/bash


#---------------------------------------------
# test runs after testbed conf. optimizations
# (ARP Retries & Cache TO, TCP SYN TO, 16 dBm 24Mbps MCS3 + Peer Link Blocking)
#---------------------------------------------

#./runAzureusMeasurement_args.sh MESH 1 5 1 1
#./runAzureusMeasurement_args.sh MESH 1 5 1 2
#./runAzureusMeasurement_args.sh MESH 1 5 1 3
#./runAzureusMeasurement_args.sh MESH 1 5 1 4
#./runAzureusMeasurement_args.sh MESH 1 5 1 5

#./runAzureusMeasurement_args.sh MESH 1 5 7 1
#./runAzureusMeasurement_args.sh MESH 1 5 7 2
#./runAzureusMeasurement_args.sh MESH 1 5 7 3
#./runAzureusMeasurement_args.sh MESH 1 5 7 4
#./runAzureusMeasurement_args.sh MESH 1 5 7 5

#./runAzureusMeasurement_args.sh MESH 1 5 13 1
#./runAzureusMeasurement_args.sh MESH 1 5 13 2
#./runAzureusMeasurement_args.sh MESH 1 5 13 3
#./runAzureusMeasurement_args.sh MESH 1 5 13 4
#./runAzureusMeasurement_args.sh MESH 1 5 13 5

#./runAzureusMeasurement_args.sh MESH 1 5 18 1
#./runAzureusMeasurement_args.sh MESH 1 5 18 2
#./runAzureusMeasurement_args.sh MESH 1 5 18 3
#./runAzureusMeasurement_args.sh MESH 1 5 18 4
#./runAzureusMeasurement_args.sh MESH 1 5 18 5

#---------------------------------------------

#./runAzureusMeasurement_args.sh MESH 2 3 1 1
#./runAzureusMeasurement_args.sh MESH 2 3 1 2
#./runAzureusMeasurement_args.sh MESH 2 3 1 3
#./runAzureusMeasurement_args.sh MESH 2 3 1 4
#./runAzureusMeasurement_args.sh MESH 2 3 1 5

#./runAzureusMeasurement_args.sh MESH 2 3 7 1
#./runAzureusMeasurement_args.sh MESH 2 3 7 2
#./runAzureusMeasurement_args.sh MESH 2 3 7 3
#./runAzureusMeasurement_args.sh MESH 2 3 7 4
#./runAzureusMeasurement_args.sh MESH 2 3 7 5

#./runAzureusMeasurement_args.sh MESH 2 3 13 1
#./runAzureusMeasurement_args.sh MESH 2 3 13 2
#./runAzureusMeasurement_args.sh MESH 2 3 13 3
#./runAzureusMeasurement_args.sh MESH 2 3 13 4
#./runAzureusMeasurement_args.sh MESH 2 3 13 5

#./runAzureusMeasurement_args.sh MESH 2 3 18 1
#./runAzureusMeasurement_args.sh MESH 2 3 18 2
#./runAzureusMeasurement_args.sh MESH 2 3 18 3
#./runAzureusMeasurement_args.sh MESH 2 3 18 4
#./runAzureusMeasurement_args.sh MESH 2 3 18 5

#---------------------------------------------

#./runAzureusMeasurement_args.sh MESH 2 4 1 1
#./runAzureusMeasurement_args.sh MESH 2 4 1 2
#./runAzureusMeasurement_args.sh MESH 2 4 1 3
#./runAzureusMeasurement_args.sh MESH 2 4 1 4
#./runAzureusMeasurement_args.sh MESH 2 4 1 5

#./runAzureusMeasurement_args.sh MESH 2 4 7 1
#./runAzureusMeasurement_args.sh MESH 2 4 7 2
#./runAzureusMeasurement_args.sh MESH 2 4 7 3
#./runAzureusMeasurement_args.sh MESH 2 4 7 4
#./runAzureusMeasurement_args.sh MESH 2 4 7 5

#./runAzureusMeasurement_args.sh MESH 2 4 13 1
#./runAzureusMeasurement_args.sh MESH 2 4 13 2
#./runAzureusMeasurement_args.sh MESH 2 4 13 3
#./runAzureusMeasurement_args.sh MESH 2 4 13 4
#./runAzureusMeasurement_args.sh MESH 2 4 13 5

#./runAzureusMeasurement_args.sh MESH 2 4 18 1
#./runAzureusMeasurement_args.sh MESH 2 4 18 2
#./runAzureusMeasurement_args.sh MESH 2 4 18 3
#./runAzureusMeasurement_args.sh MESH 2 4 18 4
#./runAzureusMeasurement_args.sh MESH 2 4 18 5

#---------------------------------------------

#./runAzureusMeasurement_args.sh MESH 2 5 1 1
#./runAzureusMeasurement_args.sh MESH 2 5 1 2
#./runAzureusMeasurement_args.sh MESH 2 5 1 3
#./runAzureusMeasurement_args.sh MESH 2 5 1 4
#./runAzureusMeasurement_args.sh MESH 2 5 1 5

#./runAzureusMeasurement_args.sh MESH 2 5 7 1
#./runAzureusMeasurement_args.sh MESH 2 5 7 2
#./runAzureusMeasurement_args.sh MESH 2 5 7 3
#./runAzureusMeasurement_args.sh MESH 2 5 7 4
#./runAzureusMeasurement_args.sh MESH 2 5 7 5

#./runAzureusMeasurement_args.sh MESH 2 5 13 1
#./runAzureusMeasurement_args.sh MESH 2 5 13 2
#./runAzureusMeasurement_args.sh MESH 2 5 13 3
#./runAzureusMeasurement_args.sh MESH 2 5 13 4
#./runAzureusMeasurement_args.sh MESH 2 5 13 5

#./runAzureusMeasurement_args.sh MESH 2 5 18 1
#./runAzureusMeasurement_args.sh MESH 2 5 18 2
#./runAzureusMeasurement_args.sh MESH 2 5 18 3
#./runAzureusMeasurement_args.sh MESH 2 5 18 4
#./runAzureusMeasurement_args.sh MESH 2 5 18 5

#---------------------------------------------



#---------------------------------------------
# broken runs
#---------------------------------------------

#./runAzureusMeasurement_args.sh MESH 1 3 12 1
#./runAzureusMeasurement_args.sh MESH 1 5 6 4
#./runAzureusMeasurement_args.sh MESH 1 5 8 5
#./runAzureusMeasurement_args.sh MESH 1 5 11 5
#./runAzureusMeasurement_args.sh MESH 1 5 12 2
#./runAzureusMeasurement_args.sh MESH 2 2 12 2
#./runAzureusMeasurement_args.sh MESH 2 4 15 5
#./runAzureusMeasurement_args.sh MESH 2 1 1 1
#./runAzureusMeasurement_args.sh MESH 2 1 10 4
#./runAzureusMeasurement_args.sh MESH 2 4 10 1
#./runAzureusMeasurement_args.sh MESH 2 4 11 3
#./runAzureusMeasurement_args.sh MESH 2 4 14 1
#./runAzureusMeasurement_args.sh MESH 2 5 5 5
#./runAzureusMeasurement_args.sh MESH 2 5 12 4

#---------------------------------------------



#---------------------------------------------
# all configurations
#---------------------------------------------

#./runAzureusMeasurement_args.sh MESH 1 1 1 1
#./runAzureusMeasurement_args.sh MESH 1 1 1 2
#./runAzureusMeasurement_args.sh MESH 1 1 1 3
#./runAzureusMeasurement_args.sh MESH 1 1 1 4
#./runAzureusMeasurement_args.sh MESH 1 1 1 5

#./runAzureusMeasurement_args.sh MESH 1 1 2 1
#./runAzureusMeasurement_args.sh MESH 1 1 2 2
#./runAzureusMeasurement_args.sh MESH 1 1 2 3
#./runAzureusMeasurement_args.sh MESH 1 1 2 4
#./runAzureusMeasurement_args.sh MESH 1 1 2 5

#./runAzureusMeasurement_args.sh MESH 1 1 3 1
#./runAzureusMeasurement_args.sh MESH 1 1 3 2
#./runAzureusMeasurement_args.sh MESH 1 1 3 3
#./runAzureusMeasurement_args.sh MESH 1 1 3 4
#./runAzureusMeasurement_args.sh MESH 1 1 3 5

#./runAzureusMeasurement_args.sh MESH 1 1 4 1
#./runAzureusMeasurement_args.sh MESH 1 1 4 2
#./runAzureusMeasurement_args.sh MESH 1 1 4 3
#./runAzureusMeasurement_args.sh MESH 1 1 4 4
#./runAzureusMeasurement_args.sh MESH 1 1 4 5

#./runAzureusMeasurement_args.sh MESH 1 1 5 1
#./runAzureusMeasurement_args.sh MESH 1 1 5 2
#./runAzureusMeasurement_args.sh MESH 1 1 5 3
#./runAzureusMeasurement_args.sh MESH 1 1 5 4
#./runAzureusMeasurement_args.sh MESH 1 1 5 5

#./runAzureusMeasurement_args.sh MESH 1 1 6 1
#./runAzureusMeasurement_args.sh MESH 1 1 6 2
#./runAzureusMeasurement_args.sh MESH 1 1 6 3
#./runAzureusMeasurement_args.sh MESH 1 1 6 4
#./runAzureusMeasurement_args.sh MESH 1 1 6 5

#./runAzureusMeasurement_args.sh MESH 1 1 7 1
#./runAzureusMeasurement_args.sh MESH 1 1 7 2
#./runAzureusMeasurement_args.sh MESH 1 1 7 3
#./runAzureusMeasurement_args.sh MESH 1 1 7 4
#./runAzureusMeasurement_args.sh MESH 1 1 7 5

#./runAzureusMeasurement_args.sh MESH 1 1 8 1
#./runAzureusMeasurement_args.sh MESH 1 1 8 2
#./runAzureusMeasurement_args.sh MESH 1 1 8 3
#./runAzureusMeasurement_args.sh MESH 1 1 8 4
#./runAzureusMeasurement_args.sh MESH 1 1 8 5

#./runAzureusMeasurement_args.sh MESH 1 1 9 1
#./runAzureusMeasurement_args.sh MESH 1 1 9 2
#./runAzureusMeasurement_args.sh MESH 1 1 9 3
#./runAzureusMeasurement_args.sh MESH 1 1 9 4
#./runAzureusMeasurement_args.sh MESH 1 1 9 5

#./runAzureusMeasurement_args.sh MESH 1 1 10 1
#./runAzureusMeasurement_args.sh MESH 1 1 10 2
#./runAzureusMeasurement_args.sh MESH 1 1 10 3
#./runAzureusMeasurement_args.sh MESH 1 1 10 4
#./runAzureusMeasurement_args.sh MESH 1 1 10 5

#./runAzureusMeasurement_args.sh MESH 1 1 11 1
#./runAzureusMeasurement_args.sh MESH 1 1 11 2
#./runAzureusMeasurement_args.sh MESH 1 1 11 3
#./runAzureusMeasurement_args.sh MESH 1 1 11 4
#./runAzureusMeasurement_args.sh MESH 1 1 11 5

#./runAzureusMeasurement_args.sh MESH 1 1 12 1
#./runAzureusMeasurement_args.sh MESH 1 1 12 2
#./runAzureusMeasurement_args.sh MESH 1 1 12 3
#./runAzureusMeasurement_args.sh MESH 1 1 12 4
#./runAzureusMeasurement_args.sh MESH 1 1 12 5

#./runAzureusMeasurement_args.sh MESH 1 1 13 1
#./runAzureusMeasurement_args.sh MESH 1 1 13 2
#./runAzureusMeasurement_args.sh MESH 1 1 13 3
#./runAzureusMeasurement_args.sh MESH 1 1 13 4
#./runAzureusMeasurement_args.sh MESH 1 1 13 5

#./runAzureusMeasurement_args.sh MESH 1 1 14 1
#./runAzureusMeasurement_args.sh MESH 1 1 14 2
#./runAzureusMeasurement_args.sh MESH 1 1 14 3
#./runAzureusMeasurement_args.sh MESH 1 1 14 4
#./runAzureusMeasurement_args.sh MESH 1 1 14 5

#./runAzureusMeasurement_args.sh MESH 1 1 15 1
#./runAzureusMeasurement_args.sh MESH 1 1 15 2
#./runAzureusMeasurement_args.sh MESH 1 1 15 3
#./runAzureusMeasurement_args.sh MESH 1 1 15 4
#./runAzureusMeasurement_args.sh MESH 1 1 15 5

#./runAzureusMeasurement_args.sh MESH 1 1 16 1
#./runAzureusMeasurement_args.sh MESH 1 1 16 2
#./runAzureusMeasurement_args.sh MESH 1 1 16 3
#./runAzureusMeasurement_args.sh MESH 1 1 16 4
#./runAzureusMeasurement_args.sh MESH 1 1 16 5

#./runAzureusMeasurement_args.sh MESH 1 1 17 1
#./runAzureusMeasurement_args.sh MESH 1 1 17 2
#./runAzureusMeasurement_args.sh MESH 1 1 17 3
#./runAzureusMeasurement_args.sh MESH 1 1 17 4
#./runAzureusMeasurement_args.sh MESH 1 1 17 5

#./runAzureusMeasurement_args.sh MESH 1 1 18 1
#./runAzureusMeasurement_args.sh MESH 1 1 18 2
#./runAzureusMeasurement_args.sh MESH 1 1 18 3
#./runAzureusMeasurement_args.sh MESH 1 1 18 4
#./runAzureusMeasurement_args.sh MESH 1 1 18 5





#./runAzureusMeasurement_args.sh MESH 1 2 1 1
#./runAzureusMeasurement_args.sh MESH 1 2 1 2
#./runAzureusMeasurement_args.sh MESH 1 2 1 3
#./runAzureusMeasurement_args.sh MESH 1 2 1 4
#./runAzureusMeasurement_args.sh MESH 1 2 1 5

#./runAzureusMeasurement_args.sh MESH 1 2 2 1
#./runAzureusMeasurement_args.sh MESH 1 2 2 2
#./runAzureusMeasurement_args.sh MESH 1 2 2 3
#./runAzureusMeasurement_args.sh MESH 1 2 2 4
#./runAzureusMeasurement_args.sh MESH 1 2 2 5

#./runAzureusMeasurement_args.sh MESH 1 2 3 1
#./runAzureusMeasurement_args.sh MESH 1 2 3 2
#./runAzureusMeasurement_args.sh MESH 1 2 3 3
#./runAzureusMeasurement_args.sh MESH 1 2 3 4
#./runAzureusMeasurement_args.sh MESH 1 2 3 5

#./runAzureusMeasurement_args.sh MESH 1 2 4 1
#./runAzureusMeasurement_args.sh MESH 1 2 4 2
#./runAzureusMeasurement_args.sh MESH 1 2 4 3
#./runAzureusMeasurement_args.sh MESH 1 2 4 4
#./runAzureusMeasurement_args.sh MESH 1 2 4 5

#./runAzureusMeasurement_args.sh MESH 1 2 5 1
#./runAzureusMeasurement_args.sh MESH 1 2 5 2
#./runAzureusMeasurement_args.sh MESH 1 2 5 3
#./runAzureusMeasurement_args.sh MESH 1 2 5 4
#./runAzureusMeasurement_args.sh MESH 1 2 5 5

#./runAzureusMeasurement_args.sh MESH 1 2 6 1
#./runAzureusMeasurement_args.sh MESH 1 2 6 2
#./runAzureusMeasurement_args.sh MESH 1 2 6 3
#./runAzureusMeasurement_args.sh MESH 1 2 6 4
#./runAzureusMeasurement_args.sh MESH 1 2 6 5

#./runAzureusMeasurement_args.sh MESH 1 2 7 1
#./runAzureusMeasurement_args.sh MESH 1 2 7 2
#./runAzureusMeasurement_args.sh MESH 1 2 7 3
#./runAzureusMeasurement_args.sh MESH 1 2 7 4
#./runAzureusMeasurement_args.sh MESH 1 2 7 5

#./runAzureusMeasurement_args.sh MESH 1 2 8 1
#./runAzureusMeasurement_args.sh MESH 1 2 8 2
#./runAzureusMeasurement_args.sh MESH 1 2 8 3
#./runAzureusMeasurement_args.sh MESH 1 2 8 4
#./runAzureusMeasurement_args.sh MESH 1 2 8 5

#./runAzureusMeasurement_args.sh MESH 1 2 9 1
#./runAzureusMeasurement_args.sh MESH 1 2 9 2
#./runAzureusMeasurement_args.sh MESH 1 2 9 3
#./runAzureusMeasurement_args.sh MESH 1 2 9 4
#./runAzureusMeasurement_args.sh MESH 1 2 9 5

#./runAzureusMeasurement_args.sh MESH 1 2 10 1
#./runAzureusMeasurement_args.sh MESH 1 2 10 2
#./runAzureusMeasurement_args.sh MESH 1 2 10 3
#./runAzureusMeasurement_args.sh MESH 1 2 10 4
#./runAzureusMeasurement_args.sh MESH 1 2 10 5

#./runAzureusMeasurement_args.sh MESH 1 2 11 1
#./runAzureusMeasurement_args.sh MESH 1 2 11 2
#./runAzureusMeasurement_args.sh MESH 1 2 11 3
#./runAzureusMeasurement_args.sh MESH 1 2 11 4
#./runAzureusMeasurement_args.sh MESH 1 2 11 5

#./runAzureusMeasurement_args.sh MESH 1 2 12 1
#./runAzureusMeasurement_args.sh MESH 1 2 12 2
#./runAzureusMeasurement_args.sh MESH 1 2 12 3
#./runAzureusMeasurement_args.sh MESH 1 2 12 4
#./runAzureusMeasurement_args.sh MESH 1 2 12 5

#./runAzureusMeasurement_args.sh MESH 1 2 13 1
#./runAzureusMeasurement_args.sh MESH 1 2 13 2
#./runAzureusMeasurement_args.sh MESH 1 2 13 3
#./runAzureusMeasurement_args.sh MESH 1 2 13 4
#./runAzureusMeasurement_args.sh MESH 1 2 13 5

#./runAzureusMeasurement_args.sh MESH 1 2 14 1
#./runAzureusMeasurement_args.sh MESH 1 2 14 2
#./runAzureusMeasurement_args.sh MESH 1 2 14 3
#./runAzureusMeasurement_args.sh MESH 1 2 14 4
#./runAzureusMeasurement_args.sh MESH 1 2 14 5

#./runAzureusMeasurement_args.sh MESH 1 2 15 1
#./runAzureusMeasurement_args.sh MESH 1 2 15 2
#./runAzureusMeasurement_args.sh MESH 1 2 15 3
#./runAzureusMeasurement_args.sh MESH 1 2 15 4
#./runAzureusMeasurement_args.sh MESH 1 2 15 5

#./runAzureusMeasurement_args.sh MESH 1 2 16 1
#./runAzureusMeasurement_args.sh MESH 1 2 16 2
#./runAzureusMeasurement_args.sh MESH 1 2 16 3
#./runAzureusMeasurement_args.sh MESH 1 2 16 4
#./runAzureusMeasurement_args.sh MESH 1 2 16 5

#./runAzureusMeasurement_args.sh MESH 1 2 17 1
#./runAzureusMeasurement_args.sh MESH 1 2 17 2
#./runAzureusMeasurement_args.sh MESH 1 2 17 3
#./runAzureusMeasurement_args.sh MESH 1 2 17 4
#./runAzureusMeasurement_args.sh MESH 1 2 17 5

#./runAzureusMeasurement_args.sh MESH 1 2 18 1
#./runAzureusMeasurement_args.sh MESH 1 2 18 2
#./runAzureusMeasurement_args.sh MESH 1 2 18 3
#./runAzureusMeasurement_args.sh MESH 1 2 18 4
#./runAzureusMeasurement_args.sh MESH 1 2 18 5





#./runAzureusMeasurement_args.sh MESH 1 3 1 1
#./runAzureusMeasurement_args.sh MESH 1 3 1 2
#./runAzureusMeasurement_args.sh MESH 1 3 1 3
#./runAzureusMeasurement_args.sh MESH 1 3 1 4
#./runAzureusMeasurement_args.sh MESH 1 3 1 5

#./runAzureusMeasurement_args.sh MESH 1 3 2 1
#./runAzureusMeasurement_args.sh MESH 1 3 2 2
#./runAzureusMeasurement_args.sh MESH 1 3 2 3
#./runAzureusMeasurement_args.sh MESH 1 3 2 4
#./runAzureusMeasurement_args.sh MESH 1 3 2 5

#./runAzureusMeasurement_args.sh MESH 1 3 3 1
#./runAzureusMeasurement_args.sh MESH 1 3 3 2
#./runAzureusMeasurement_args.sh MESH 1 3 3 3
#./runAzureusMeasurement_args.sh MESH 1 3 3 4
#./runAzureusMeasurement_args.sh MESH 1 3 3 5

#./runAzureusMeasurement_args.sh MESH 1 3 4 1
#./runAzureusMeasurement_args.sh MESH 1 3 4 2
#./runAzureusMeasurement_args.sh MESH 1 3 4 3
#./runAzureusMeasurement_args.sh MESH 1 3 4 4
#./runAzureusMeasurement_args.sh MESH 1 3 4 5

#./runAzureusMeasurement_args.sh MESH 1 3 5 1
#./runAzureusMeasurement_args.sh MESH 1 3 5 2
#./runAzureusMeasurement_args.sh MESH 1 3 5 3
#./runAzureusMeasurement_args.sh MESH 1 3 5 4
#./runAzureusMeasurement_args.sh MESH 1 3 5 5

#./runAzureusMeasurement_args.sh MESH 1 3 6 1
#./runAzureusMeasurement_args.sh MESH 1 3 6 2
#./runAzureusMeasurement_args.sh MESH 1 3 6 3
#./runAzureusMeasurement_args.sh MESH 1 3 6 4
#./runAzureusMeasurement_args.sh MESH 1 3 6 5

#./runAzureusMeasurement_args.sh MESH 1 3 7 1
#./runAzureusMeasurement_args.sh MESH 1 3 7 2
#./runAzureusMeasurement_args.sh MESH 1 3 7 3
#./runAzureusMeasurement_args.sh MESH 1 3 7 4
#./runAzureusMeasurement_args.sh MESH 1 3 7 5

#./runAzureusMeasurement_args.sh MESH 1 3 8 1
#./runAzureusMeasurement_args.sh MESH 1 3 8 2
#./runAzureusMeasurement_args.sh MESH 1 3 8 3
#./runAzureusMeasurement_args.sh MESH 1 3 8 4
#./runAzureusMeasurement_args.sh MESH 1 3 8 5

#./runAzureusMeasurement_args.sh MESH 1 3 9 1
#./runAzureusMeasurement_args.sh MESH 1 3 9 2
#./runAzureusMeasurement_args.sh MESH 1 3 9 3
#./runAzureusMeasurement_args.sh MESH 1 3 9 4
#./runAzureusMeasurement_args.sh MESH 1 3 9 5

#./runAzureusMeasurement_args.sh MESH 1 3 10 1
#./runAzureusMeasurement_args.sh MESH 1 3 10 2
#./runAzureusMeasurement_args.sh MESH 1 3 10 3
#./runAzureusMeasurement_args.sh MESH 1 3 10 4
#./runAzureusMeasurement_args.sh MESH 1 3 10 5

#./runAzureusMeasurement_args.sh MESH 1 3 11 1
#./runAzureusMeasurement_args.sh MESH 1 3 11 2
#./runAzureusMeasurement_args.sh MESH 1 3 11 3
#./runAzureusMeasurement_args.sh MESH 1 3 11 4
#./runAzureusMeasurement_args.sh MESH 1 3 11 5

#./runAzureusMeasurement_args.sh MESH 1 3 12 1
#./runAzureusMeasurement_args.sh MESH 1 3 12 2
#./runAzureusMeasurement_args.sh MESH 1 3 12 3
#./runAzureusMeasurement_args.sh MESH 1 3 12 4
#./runAzureusMeasurement_args.sh MESH 1 3 12 5

#./runAzureusMeasurement_args.sh MESH 1 3 13 1
#./runAzureusMeasurement_args.sh MESH 1 3 13 2
#./runAzureusMeasurement_args.sh MESH 1 3 13 3
#./runAzureusMeasurement_args.sh MESH 1 3 13 4
#./runAzureusMeasurement_args.sh MESH 1 3 13 5

#./runAzureusMeasurement_args.sh MESH 1 3 14 1
#./runAzureusMeasurement_args.sh MESH 1 3 14 2
#./runAzureusMeasurement_args.sh MESH 1 3 14 3
#./runAzureusMeasurement_args.sh MESH 1 3 14 4
#./runAzureusMeasurement_args.sh MESH 1 3 14 5

#./runAzureusMeasurement_args.sh MESH 1 3 15 1
#./runAzureusMeasurement_args.sh MESH 1 3 15 2
#./runAzureusMeasurement_args.sh MESH 1 3 15 3
#./runAzureusMeasurement_args.sh MESH 1 3 15 4
#./runAzureusMeasurement_args.sh MESH 1 3 15 5

#./runAzureusMeasurement_args.sh MESH 1 3 16 1
#./runAzureusMeasurement_args.sh MESH 1 3 16 2
#./runAzureusMeasurement_args.sh MESH 1 3 16 3
#./runAzureusMeasurement_args.sh MESH 1 3 16 4
#./runAzureusMeasurement_args.sh MESH 1 3 16 5

#./runAzureusMeasurement_args.sh MESH 1 3 17 1
#./runAzureusMeasurement_args.sh MESH 1 3 17 2
#./runAzureusMeasurement_args.sh MESH 1 3 17 3
#./runAzureusMeasurement_args.sh MESH 1 3 17 4
#./runAzureusMeasurement_args.sh MESH 1 3 17 5

#./runAzureusMeasurement_args.sh MESH 1 3 18 1
#./runAzureusMeasurement_args.sh MESH 1 3 18 2
#./runAzureusMeasurement_args.sh MESH 1 3 18 3
#./runAzureusMeasurement_args.sh MESH 1 3 18 4
#./runAzureusMeasurement_args.sh MESH 1 3 18 5





#./runAzureusMeasurement_args.sh MESH 1 4 1 1
#./runAzureusMeasurement_args.sh MESH 1 4 1 2
#./runAzureusMeasurement_args.sh MESH 1 4 1 3
#./runAzureusMeasurement_args.sh MESH 1 4 1 4
#./runAzureusMeasurement_args.sh MESH 1 4 1 5

#./runAzureusMeasurement_args.sh MESH 1 4 2 1
#./runAzureusMeasurement_args.sh MESH 1 4 2 2
#./runAzureusMeasurement_args.sh MESH 1 4 2 3
#./runAzureusMeasurement_args.sh MESH 1 4 2 4
#./runAzureusMeasurement_args.sh MESH 1 4 2 5

#./runAzureusMeasurement_args.sh MESH 1 4 3 1
#./runAzureusMeasurement_args.sh MESH 1 4 3 2
#./runAzureusMeasurement_args.sh MESH 1 4 3 3
#./runAzureusMeasurement_args.sh MESH 1 4 3 4
#./runAzureusMeasurement_args.sh MESH 1 4 3 5

#./runAzureusMeasurement_args.sh MESH 1 4 4 1
#./runAzureusMeasurement_args.sh MESH 1 4 4 2
#./runAzureusMeasurement_args.sh MESH 1 4 4 3
#./runAzureusMeasurement_args.sh MESH 1 4 4 4
#./runAzureusMeasurement_args.sh MESH 1 4 4 5

#./runAzureusMeasurement_args.sh MESH 1 4 5 1
#./runAzureusMeasurement_args.sh MESH 1 4 5 2
#./runAzureusMeasurement_args.sh MESH 1 4 5 3
#./runAzureusMeasurement_args.sh MESH 1 4 5 4
#./runAzureusMeasurement_args.sh MESH 1 4 5 5

#./runAzureusMeasurement_args.sh MESH 1 4 6 1
#./runAzureusMeasurement_args.sh MESH 1 4 6 2
#./runAzureusMeasurement_args.sh MESH 1 4 6 3
#./runAzureusMeasurement_args.sh MESH 1 4 6 4
#./runAzureusMeasurement_args.sh MESH 1 4 6 5

#./runAzureusMeasurement_args.sh MESH 1 4 7 1
#./runAzureusMeasurement_args.sh MESH 1 4 7 2
#./runAzureusMeasurement_args.sh MESH 1 4 7 3
#./runAzureusMeasurement_args.sh MESH 1 4 7 4
#./runAzureusMeasurement_args.sh MESH 1 4 7 5

#./runAzureusMeasurement_args.sh MESH 1 4 8 1
#./runAzureusMeasurement_args.sh MESH 1 4 8 2
#./runAzureusMeasurement_args.sh MESH 1 4 8 3
#./runAzureusMeasurement_args.sh MESH 1 4 8 4
#./runAzureusMeasurement_args.sh MESH 1 4 8 5

#./runAzureusMeasurement_args.sh MESH 1 4 9 1
#./runAzureusMeasurement_args.sh MESH 1 4 9 2
#./runAzureusMeasurement_args.sh MESH 1 4 9 3
#./runAzureusMeasurement_args.sh MESH 1 4 9 4
#./runAzureusMeasurement_args.sh MESH 1 4 9 5

#./runAzureusMeasurement_args.sh MESH 1 4 10 1
#./runAzureusMeasurement_args.sh MESH 1 4 10 2
#./runAzureusMeasurement_args.sh MESH 1 4 10 3
#./runAzureusMeasurement_args.sh MESH 1 4 10 4
#./runAzureusMeasurement_args.sh MESH 1 4 10 5

#./runAzureusMeasurement_args.sh MESH 1 4 11 1
#./runAzureusMeasurement_args.sh MESH 1 4 11 2
#./runAzureusMeasurement_args.sh MESH 1 4 11 3
#./runAzureusMeasurement_args.sh MESH 1 4 11 4
#./runAzureusMeasurement_args.sh MESH 1 4 11 5

#./runAzureusMeasurement_args.sh MESH 1 4 12 1
#./runAzureusMeasurement_args.sh MESH 1 4 12 2
#./runAzureusMeasurement_args.sh MESH 1 4 12 3
#./runAzureusMeasurement_args.sh MESH 1 4 12 4
#./runAzureusMeasurement_args.sh MESH 1 4 12 5

#./runAzureusMeasurement_args.sh MESH 1 4 13 1
#./runAzureusMeasurement_args.sh MESH 1 4 13 2
#./runAzureusMeasurement_args.sh MESH 1 4 13 3
#./runAzureusMeasurement_args.sh MESH 1 4 13 4
#./runAzureusMeasurement_args.sh MESH 1 4 13 5

#./runAzureusMeasurement_args.sh MESH 1 4 14 1
#./runAzureusMeasurement_args.sh MESH 1 4 14 2
#./runAzureusMeasurement_args.sh MESH 1 4 14 3
#./runAzureusMeasurement_args.sh MESH 1 4 14 4
#./runAzureusMeasurement_args.sh MESH 1 4 14 5

#./runAzureusMeasurement_args.sh MESH 1 4 15 1
#./runAzureusMeasurement_args.sh MESH 1 4 15 2
#./runAzureusMeasurement_args.sh MESH 1 4 15 3
#./runAzureusMeasurement_args.sh MESH 1 4 15 4
#./runAzureusMeasurement_args.sh MESH 1 4 15 5

#./runAzureusMeasurement_args.sh MESH 1 4 16 1
#./runAzureusMeasurement_args.sh MESH 1 4 16 2
#./runAzureusMeasurement_args.sh MESH 1 4 16 3
#./runAzureusMeasurement_args.sh MESH 1 4 16 4
#./runAzureusMeasurement_args.sh MESH 1 4 16 5

#./runAzureusMeasurement_args.sh MESH 1 4 17 1
#./runAzureusMeasurement_args.sh MESH 1 4 17 2
#./runAzureusMeasurement_args.sh MESH 1 4 17 3
#./runAzureusMeasurement_args.sh MESH 1 4 17 4
#./runAzureusMeasurement_args.sh MESH 1 4 17 5

#./runAzureusMeasurement_args.sh MESH 1 4 18 1
#./runAzureusMeasurement_args.sh MESH 1 4 18 2
#./runAzureusMeasurement_args.sh MESH 1 4 18 3
#./runAzureusMeasurement_args.sh MESH 1 4 18 4
#./runAzureusMeasurement_args.sh MESH 1 4 18 5





#./runAzureusMeasurement_args.sh MESH 1 5 1 1
#./runAzureusMeasurement_args.sh MESH 1 5 1 2
#./runAzureusMeasurement_args.sh MESH 1 5 1 3
#./runAzureusMeasurement_args.sh MESH 1 5 1 4
#./runAzureusMeasurement_args.sh MESH 1 5 1 5

#./runAzureusMeasurement_args.sh MESH 1 5 2 1
#./runAzureusMeasurement_args.sh MESH 1 5 2 2
#./runAzureusMeasurement_args.sh MESH 1 5 2 3
#./runAzureusMeasurement_args.sh MESH 1 5 2 4
#./runAzureusMeasurement_args.sh MESH 1 5 2 5

#./runAzureusMeasurement_args.sh MESH 1 5 3 1
#./runAzureusMeasurement_args.sh MESH 1 5 3 2
#./runAzureusMeasurement_args.sh MESH 1 5 3 3
#./runAzureusMeasurement_args.sh MESH 1 5 3 4
#./runAzureusMeasurement_args.sh MESH 1 5 3 5

#./runAzureusMeasurement_args.sh MESH 1 5 4 1
#./runAzureusMeasurement_args.sh MESH 1 5 4 2
#./runAzureusMeasurement_args.sh MESH 1 5 4 3
#./runAzureusMeasurement_args.sh MESH 1 5 4 4
#./runAzureusMeasurement_args.sh MESH 1 5 4 5

#./runAzureusMeasurement_args.sh MESH 1 5 5 1
#./runAzureusMeasurement_args.sh MESH 1 5 5 2
#./runAzureusMeasurement_args.sh MESH 1 5 5 3
#./runAzureusMeasurement_args.sh MESH 1 5 5 4
#./runAzureusMeasurement_args.sh MESH 1 5 5 5

#./runAzureusMeasurement_args.sh MESH 1 5 6 1
#./runAzureusMeasurement_args.sh MESH 1 5 6 2
#./runAzureusMeasurement_args.sh MESH 1 5 6 3
#./runAzureusMeasurement_args.sh MESH 1 5 6 4
#./runAzureusMeasurement_args.sh MESH 1 5 6 5

#./runAzureusMeasurement_args.sh MESH 1 5 7 1
#./runAzureusMeasurement_args.sh MESH 1 5 7 2
#./runAzureusMeasurement_args.sh MESH 1 5 7 3
#./runAzureusMeasurement_args.sh MESH 1 5 7 4
#./runAzureusMeasurement_args.sh MESH 1 5 7 5

#./runAzureusMeasurement_args.sh MESH 1 5 8 1
#./runAzureusMeasurement_args.sh MESH 1 5 8 2
#./runAzureusMeasurement_args.sh MESH 1 5 8 3
#./runAzureusMeasurement_args.sh MESH 1 5 8 4
#./runAzureusMeasurement_args.sh MESH 1 5 8 5

#./runAzureusMeasurement_args.sh MESH 1 5 9 1
#./runAzureusMeasurement_args.sh MESH 1 5 9 2
#./runAzureusMeasurement_args.sh MESH 1 5 9 3
#./runAzureusMeasurement_args.sh MESH 1 5 9 4
#./runAzureusMeasurement_args.sh MESH 1 5 9 5

#./runAzureusMeasurement_args.sh MESH 1 5 10 1
#./runAzureusMeasurement_args.sh MESH 1 5 10 2
#./runAzureusMeasurement_args.sh MESH 1 5 10 3
#./runAzureusMeasurement_args.sh MESH 1 5 10 4
#./runAzureusMeasurement_args.sh MESH 1 5 10 5

#./runAzureusMeasurement_args.sh MESH 1 5 11 1
#./runAzureusMeasurement_args.sh MESH 1 5 11 2
#./runAzureusMeasurement_args.sh MESH 1 5 11 3
#./runAzureusMeasurement_args.sh MESH 1 5 11 4
#./runAzureusMeasurement_args.sh MESH 1 5 11 5

#./runAzureusMeasurement_args.sh MESH 1 5 12 1
#./runAzureusMeasurement_args.sh MESH 1 5 12 2
#./runAzureusMeasurement_args.sh MESH 1 5 12 3
#./runAzureusMeasurement_args.sh MESH 1 5 12 4
#./runAzureusMeasurement_args.sh MESH 1 5 12 5

#./runAzureusMeasurement_args.sh MESH 1 5 13 1
#./runAzureusMeasurement_args.sh MESH 1 5 13 2
#./runAzureusMeasurement_args.sh MESH 1 5 13 3
#./runAzureusMeasurement_args.sh MESH 1 5 13 4
#./runAzureusMeasurement_args.sh MESH 1 5 13 5

#./runAzureusMeasurement_args.sh MESH 1 5 14 1
#./runAzureusMeasurement_args.sh MESH 1 5 14 2
#./runAzureusMeasurement_args.sh MESH 1 5 14 3
#./runAzureusMeasurement_args.sh MESH 1 5 14 4
#./runAzureusMeasurement_args.sh MESH 1 5 14 5

#./runAzureusMeasurement_args.sh MESH 1 5 15 1
#./runAzureusMeasurement_args.sh MESH 1 5 15 2
#./runAzureusMeasurement_args.sh MESH 1 5 15 3
#./runAzureusMeasurement_args.sh MESH 1 5 15 4
#./runAzureusMeasurement_args.sh MESH 1 5 15 5

#./runAzureusMeasurement_args.sh MESH 1 5 16 1
#./runAzureusMeasurement_args.sh MESH 1 5 16 2
#./runAzureusMeasurement_args.sh MESH 1 5 16 3
#./runAzureusMeasurement_args.sh MESH 1 5 16 4
#./runAzureusMeasurement_args.sh MESH 1 5 16 5

#./runAzureusMeasurement_args.sh MESH 1 5 17 1
#./runAzureusMeasurement_args.sh MESH 1 5 17 2
#./runAzureusMeasurement_args.sh MESH 1 5 17 3
#./runAzureusMeasurement_args.sh MESH 1 5 17 4
#./runAzureusMeasurement_args.sh MESH 1 5 17 5

#./runAzureusMeasurement_args.sh MESH 1 5 18 1
#./runAzureusMeasurement_args.sh MESH 1 5 18 2
#./runAzureusMeasurement_args.sh MESH 1 5 18 3
#./runAzureusMeasurement_args.sh MESH 1 5 18 4
#./runAzureusMeasurement_args.sh MESH 1 5 18 5





#./runAzureusMeasurement_args.sh MESH 2 1 1 1
#./runAzureusMeasurement_args.sh MESH 2 1 1 2
#./runAzureusMeasurement_args.sh MESH 2 1 1 3
#./runAzureusMeasurement_args.sh MESH 2 1 1 4
#./runAzureusMeasurement_args.sh MESH 2 1 1 5

#./runAzureusMeasurement_args.sh MESH 2 1 2 1
#./runAzureusMeasurement_args.sh MESH 2 1 2 2
#./runAzureusMeasurement_args.sh MESH 2 1 2 3
#./runAzureusMeasurement_args.sh MESH 2 1 2 4
#./runAzureusMeasurement_args.sh MESH 2 1 2 5

#./runAzureusMeasurement_args.sh MESH 2 1 3 1
#./runAzureusMeasurement_args.sh MESH 2 1 3 2
#./runAzureusMeasurement_args.sh MESH 2 1 3 3
#./runAzureusMeasurement_args.sh MESH 2 1 3 4
#./runAzureusMeasurement_args.sh MESH 2 1 3 5

#./runAzureusMeasurement_args.sh MESH 2 1 4 1
#./runAzureusMeasurement_args.sh MESH 2 1 4 2
#./runAzureusMeasurement_args.sh MESH 2 1 4 3
#./runAzureusMeasurement_args.sh MESH 2 1 4 4
#./runAzureusMeasurement_args.sh MESH 2 1 4 5

#./runAzureusMeasurement_args.sh MESH 2 1 5 1
#./runAzureusMeasurement_args.sh MESH 2 1 5 2
#./runAzureusMeasurement_args.sh MESH 2 1 5 3
#./runAzureusMeasurement_args.sh MESH 2 1 5 4
#./runAzureusMeasurement_args.sh MESH 2 1 5 5

#./runAzureusMeasurement_args.sh MESH 2 1 6 1
#./runAzureusMeasurement_args.sh MESH 2 1 6 2
#./runAzureusMeasurement_args.sh MESH 2 1 6 3
#./runAzureusMeasurement_args.sh MESH 2 1 6 4
#./runAzureusMeasurement_args.sh MESH 2 1 6 5

#./runAzureusMeasurement_args.sh MESH 2 1 7 1
#./runAzureusMeasurement_args.sh MESH 2 1 7 2
#./runAzureusMeasurement_args.sh MESH 2 1 7 3
#./runAzureusMeasurement_args.sh MESH 2 1 7 4
#./runAzureusMeasurement_args.sh MESH 2 1 7 5

#./runAzureusMeasurement_args.sh MESH 2 1 8 1
#./runAzureusMeasurement_args.sh MESH 2 1 8 2
#./runAzureusMeasurement_args.sh MESH 2 1 8 3
#./runAzureusMeasurement_args.sh MESH 2 1 8 4
#./runAzureusMeasurement_args.sh MESH 2 1 8 5

#./runAzureusMeasurement_args.sh MESH 2 1 9 1
#./runAzureusMeasurement_args.sh MESH 2 1 9 2
#./runAzureusMeasurement_args.sh MESH 2 1 9 3
#./runAzureusMeasurement_args.sh MESH 2 1 9 4
#./runAzureusMeasurement_args.sh MESH 2 1 9 5

#./runAzureusMeasurement_args.sh MESH 2 1 10 1
#./runAzureusMeasurement_args.sh MESH 2 1 10 2
#./runAzureusMeasurement_args.sh MESH 2 1 10 3
#./runAzureusMeasurement_args.sh MESH 2 1 10 4
#./runAzureusMeasurement_args.sh MESH 2 1 10 5

#./runAzureusMeasurement_args.sh MESH 2 1 11 1
#./runAzureusMeasurement_args.sh MESH 2 1 11 2
#./runAzureusMeasurement_args.sh MESH 2 1 11 3
#./runAzureusMeasurement_args.sh MESH 2 1 11 4
#./runAzureusMeasurement_args.sh MESH 2 1 11 5

#./runAzureusMeasurement_args.sh MESH 2 1 12 1
#./runAzureusMeasurement_args.sh MESH 2 1 12 2
#./runAzureusMeasurement_args.sh MESH 2 1 12 3
#./runAzureusMeasurement_args.sh MESH 2 1 12 4
#./runAzureusMeasurement_args.sh MESH 2 1 12 5

#./runAzureusMeasurement_args.sh MESH 2 1 13 1
#./runAzureusMeasurement_args.sh MESH 2 1 13 2
#./runAzureusMeasurement_args.sh MESH 2 1 13 3
#./runAzureusMeasurement_args.sh MESH 2 1 13 4
#./runAzureusMeasurement_args.sh MESH 2 1 13 5

#./runAzureusMeasurement_args.sh MESH 2 1 14 1
#./runAzureusMeasurement_args.sh MESH 2 1 14 2
#./runAzureusMeasurement_args.sh MESH 2 1 14 3
#./runAzureusMeasurement_args.sh MESH 2 1 14 4
#./runAzureusMeasurement_args.sh MESH 2 1 14 5

#./runAzureusMeasurement_args.sh MESH 2 1 15 1
#./runAzureusMeasurement_args.sh MESH 2 1 15 2
#./runAzureusMeasurement_args.sh MESH 2 1 15 3
#./runAzureusMeasurement_args.sh MESH 2 1 15 4
#./runAzureusMeasurement_args.sh MESH 2 1 15 5

#./runAzureusMeasurement_args.sh MESH 2 1 16 1
#./runAzureusMeasurement_args.sh MESH 2 1 16 2
#./runAzureusMeasurement_args.sh MESH 2 1 16 3
#./runAzureusMeasurement_args.sh MESH 2 1 16 4
#./runAzureusMeasurement_args.sh MESH 2 1 16 5

#./runAzureusMeasurement_args.sh MESH 2 1 17 1
#./runAzureusMeasurement_args.sh MESH 2 1 17 2
#./runAzureusMeasurement_args.sh MESH 2 1 17 3
#./runAzureusMeasurement_args.sh MESH 2 1 17 4
#./runAzureusMeasurement_args.sh MESH 2 1 17 5

#./runAzureusMeasurement_args.sh MESH 2 1 18 1
#./runAzureusMeasurement_args.sh MESH 2 1 18 2
#./runAzureusMeasurement_args.sh MESH 2 1 18 3
#./runAzureusMeasurement_args.sh MESH 2 1 18 4
#./runAzureusMeasurement_args.sh MESH 2 1 18 5





#./runAzureusMeasurement_args.sh MESH 2 2 1 1
#./runAzureusMeasurement_args.sh MESH 2 2 1 2
#./runAzureusMeasurement_args.sh MESH 2 2 1 3
#./runAzureusMeasurement_args.sh MESH 2 2 1 4
#./runAzureusMeasurement_args.sh MESH 2 2 1 5

#./runAzureusMeasurement_args.sh MESH 2 2 2 1
#./runAzureusMeasurement_args.sh MESH 2 2 2 2
#./runAzureusMeasurement_args.sh MESH 2 2 2 3
#./runAzureusMeasurement_args.sh MESH 2 2 2 4
#./runAzureusMeasurement_args.sh MESH 2 2 2 5

#./runAzureusMeasurement_args.sh MESH 2 2 3 1
#./runAzureusMeasurement_args.sh MESH 2 2 3 2
#./runAzureusMeasurement_args.sh MESH 2 2 3 3
#./runAzureusMeasurement_args.sh MESH 2 2 3 4
#./runAzureusMeasurement_args.sh MESH 2 2 3 5

#./runAzureusMeasurement_args.sh MESH 2 2 4 1
#./runAzureusMeasurement_args.sh MESH 2 2 4 2
#./runAzureusMeasurement_args.sh MESH 2 2 4 3
#./runAzureusMeasurement_args.sh MESH 2 2 4 4
#./runAzureusMeasurement_args.sh MESH 2 2 4 5

#./runAzureusMeasurement_args.sh MESH 2 2 5 1
#./runAzureusMeasurement_args.sh MESH 2 2 5 2
#./runAzureusMeasurement_args.sh MESH 2 2 5 3
#./runAzureusMeasurement_args.sh MESH 2 2 5 4
#./runAzureusMeasurement_args.sh MESH 2 2 5 5

#./runAzureusMeasurement_args.sh MESH 2 2 6 1
#./runAzureusMeasurement_args.sh MESH 2 2 6 2
#./runAzureusMeasurement_args.sh MESH 2 2 6 3
#./runAzureusMeasurement_args.sh MESH 2 2 6 4
#./runAzureusMeasurement_args.sh MESH 2 2 6 5

#./runAzureusMeasurement_args.sh MESH 2 2 7 1
#./runAzureusMeasurement_args.sh MESH 2 2 7 2
#./runAzureusMeasurement_args.sh MESH 2 2 7 3
#./runAzureusMeasurement_args.sh MESH 2 2 7 4
#./runAzureusMeasurement_args.sh MESH 2 2 7 5

#./runAzureusMeasurement_args.sh MESH 2 2 8 1
#./runAzureusMeasurement_args.sh MESH 2 2 8 2
#./runAzureusMeasurement_args.sh MESH 2 2 8 3
#./runAzureusMeasurement_args.sh MESH 2 2 8 4
#./runAzureusMeasurement_args.sh MESH 2 2 8 5

#./runAzureusMeasurement_args.sh MESH 2 2 9 1
#./runAzureusMeasurement_args.sh MESH 2 2 9 2
#./runAzureusMeasurement_args.sh MESH 2 2 9 3
#./runAzureusMeasurement_args.sh MESH 2 2 9 4
#./runAzureusMeasurement_args.sh MESH 2 2 9 5

#./runAzureusMeasurement_args.sh MESH 2 2 10 1
#./runAzureusMeasurement_args.sh MESH 2 2 10 2
#./runAzureusMeasurement_args.sh MESH 2 2 10 3
#./runAzureusMeasurement_args.sh MESH 2 2 10 4
#./runAzureusMeasurement_args.sh MESH 2 2 10 5

#./runAzureusMeasurement_args.sh MESH 2 2 11 1
#./runAzureusMeasurement_args.sh MESH 2 2 11 2
#./runAzureusMeasurement_args.sh MESH 2 2 11 3
#./runAzureusMeasurement_args.sh MESH 2 2 11 4
#./runAzureusMeasurement_args.sh MESH 2 2 11 5

#./runAzureusMeasurement_args.sh MESH 2 2 12 1
#./runAzureusMeasurement_args.sh MESH 2 2 12 2
#./runAzureusMeasurement_args.sh MESH 2 2 12 3
#./runAzureusMeasurement_args.sh MESH 2 2 12 4
#./runAzureusMeasurement_args.sh MESH 2 2 12 5

#./runAzureusMeasurement_args.sh MESH 2 2 13 1
#./runAzureusMeasurement_args.sh MESH 2 2 13 2
#./runAzureusMeasurement_args.sh MESH 2 2 13 3
#./runAzureusMeasurement_args.sh MESH 2 2 13 4
#./runAzureusMeasurement_args.sh MESH 2 2 13 5

#./runAzureusMeasurement_args.sh MESH 2 2 14 1
#./runAzureusMeasurement_args.sh MESH 2 2 14 2
#./runAzureusMeasurement_args.sh MESH 2 2 14 3
#./runAzureusMeasurement_args.sh MESH 2 2 14 4
#./runAzureusMeasurement_args.sh MESH 2 2 14 5

#./runAzureusMeasurement_args.sh MESH 2 2 15 1
#./runAzureusMeasurement_args.sh MESH 2 2 15 2
#./runAzureusMeasurement_args.sh MESH 2 2 15 3
#./runAzureusMeasurement_args.sh MESH 2 2 15 4
#./runAzureusMeasurement_args.sh MESH 2 2 15 5

#./runAzureusMeasurement_args.sh MESH 2 2 16 1
#./runAzureusMeasurement_args.sh MESH 2 2 16 2
#./runAzureusMeasurement_args.sh MESH 2 2 16 3
#./runAzureusMeasurement_args.sh MESH 2 2 16 4
#./runAzureusMeasurement_args.sh MESH 2 2 16 5

#./runAzureusMeasurement_args.sh MESH 2 2 17 1
#./runAzureusMeasurement_args.sh MESH 2 2 17 2
#./runAzureusMeasurement_args.sh MESH 2 2 17 3
#./runAzureusMeasurement_args.sh MESH 2 2 17 4
#./runAzureusMeasurement_args.sh MESH 2 2 17 5

#./runAzureusMeasurement_args.sh MESH 2 2 18 1
#./runAzureusMeasurement_args.sh MESH 2 2 18 2
#./runAzureusMeasurement_args.sh MESH 2 2 18 3
#./runAzureusMeasurement_args.sh MESH 2 2 18 4
#./runAzureusMeasurement_args.sh MESH 2 2 18 5





#./runAzureusMeasurement_args.sh MESH 2 3 1 1
#./runAzureusMeasurement_args.sh MESH 2 3 1 2
#./runAzureusMeasurement_args.sh MESH 2 3 1 3
#./runAzureusMeasurement_args.sh MESH 2 3 1 4
#./runAzureusMeasurement_args.sh MESH 2 3 1 5

#./runAzureusMeasurement_args.sh MESH 2 3 2 1
#./runAzureusMeasurement_args.sh MESH 2 3 2 2
#./runAzureusMeasurement_args.sh MESH 2 3 2 3
#./runAzureusMeasurement_args.sh MESH 2 3 2 4
#./runAzureusMeasurement_args.sh MESH 2 3 2 5

#./runAzureusMeasurement_args.sh MESH 2 3 3 1
#./runAzureusMeasurement_args.sh MESH 2 3 3 2
#./runAzureusMeasurement_args.sh MESH 2 3 3 3
#./runAzureusMeasurement_args.sh MESH 2 3 3 4
#./runAzureusMeasurement_args.sh MESH 2 3 3 5

#./runAzureusMeasurement_args.sh MESH 2 3 4 1
#./runAzureusMeasurement_args.sh MESH 2 3 4 2
#./runAzureusMeasurement_args.sh MESH 2 3 4 3
#./runAzureusMeasurement_args.sh MESH 2 3 4 4
#./runAzureusMeasurement_args.sh MESH 2 3 4 5

#./runAzureusMeasurement_args.sh MESH 2 3 5 1
#./runAzureusMeasurement_args.sh MESH 2 3 5 2
#./runAzureusMeasurement_args.sh MESH 2 3 5 3
#./runAzureusMeasurement_args.sh MESH 2 3 5 4
#./runAzureusMeasurement_args.sh MESH 2 3 5 5

#./runAzureusMeasurement_args.sh MESH 2 3 6 1
#./runAzureusMeasurement_args.sh MESH 2 3 6 2
#./runAzureusMeasurement_args.sh MESH 2 3 6 3
#./runAzureusMeasurement_args.sh MESH 2 3 6 4
#./runAzureusMeasurement_args.sh MESH 2 3 6 5

#./runAzureusMeasurement_args.sh MESH 2 3 7 1
#./runAzureusMeasurement_args.sh MESH 2 3 7 2
#./runAzureusMeasurement_args.sh MESH 2 3 7 3
#./runAzureusMeasurement_args.sh MESH 2 3 7 4
#./runAzureusMeasurement_args.sh MESH 2 3 7 5

#./runAzureusMeasurement_args.sh MESH 2 3 8 1
#./runAzureusMeasurement_args.sh MESH 2 3 8 2
#./runAzureusMeasurement_args.sh MESH 2 3 8 3
#./runAzureusMeasurement_args.sh MESH 2 3 8 4
#./runAzureusMeasurement_args.sh MESH 2 3 8 5

#./runAzureusMeasurement_args.sh MESH 2 3 9 1
#./runAzureusMeasurement_args.sh MESH 2 3 9 2
#./runAzureusMeasurement_args.sh MESH 2 3 9 3
#./runAzureusMeasurement_args.sh MESH 2 3 9 4
#./runAzureusMeasurement_args.sh MESH 2 3 9 5

#./runAzureusMeasurement_args.sh MESH 2 3 10 1
#./runAzureusMeasurement_args.sh MESH 2 3 10 2
#./runAzureusMeasurement_args.sh MESH 2 3 10 3
#./runAzureusMeasurement_args.sh MESH 2 3 10 4
#./runAzureusMeasurement_args.sh MESH 2 3 10 5

#./runAzureusMeasurement_args.sh MESH 2 3 11 1
#./runAzureusMeasurement_args.sh MESH 2 3 11 2
#./runAzureusMeasurement_args.sh MESH 2 3 11 3
#./runAzureusMeasurement_args.sh MESH 2 3 11 4
#./runAzureusMeasurement_args.sh MESH 2 3 11 5

#./runAzureusMeasurement_args.sh MESH 2 3 12 1
#./runAzureusMeasurement_args.sh MESH 2 3 12 2
#./runAzureusMeasurement_args.sh MESH 2 3 12 3
#./runAzureusMeasurement_args.sh MESH 2 3 12 4
#./runAzureusMeasurement_args.sh MESH 2 3 12 5

#./runAzureusMeasurement_args.sh MESH 2 3 13 1
#./runAzureusMeasurement_args.sh MESH 2 3 13 2
#./runAzureusMeasurement_args.sh MESH 2 3 13 3
#./runAzureusMeasurement_args.sh MESH 2 3 13 4
#./runAzureusMeasurement_args.sh MESH 2 3 13 5

#./runAzureusMeasurement_args.sh MESH 2 3 14 1
#./runAzureusMeasurement_args.sh MESH 2 3 14 2
#./runAzureusMeasurement_args.sh MESH 2 3 14 3
#./runAzureusMeasurement_args.sh MESH 2 3 14 4
#./runAzureusMeasurement_args.sh MESH 2 3 14 5

#./runAzureusMeasurement_args.sh MESH 2 3 15 1
#./runAzureusMeasurement_args.sh MESH 2 3 15 2
#./runAzureusMeasurement_args.sh MESH 2 3 15 3
#./runAzureusMeasurement_args.sh MESH 2 3 15 4
#./runAzureusMeasurement_args.sh MESH 2 3 15 5

#./runAzureusMeasurement_args.sh MESH 2 3 16 1
#./runAzureusMeasurement_args.sh MESH 2 3 16 2
#./runAzureusMeasurement_args.sh MESH 2 3 16 3
#./runAzureusMeasurement_args.sh MESH 2 3 16 4
#./runAzureusMeasurement_args.sh MESH 2 3 16 5

#./runAzureusMeasurement_args.sh MESH 2 3 17 1
#./runAzureusMeasurement_args.sh MESH 2 3 17 2
#./runAzureusMeasurement_args.sh MESH 2 3 17 3
#./runAzureusMeasurement_args.sh MESH 2 3 17 4
#./runAzureusMeasurement_args.sh MESH 2 3 17 5

#./runAzureusMeasurement_args.sh MESH 2 3 18 1
#./runAzureusMeasurement_args.sh MESH 2 3 18 2
#./runAzureusMeasurement_args.sh MESH 2 3 18 3
#./runAzureusMeasurement_args.sh MESH 2 3 18 4
#./runAzureusMeasurement_args.sh MESH 2 3 18 5





#./runAzureusMeasurement_args.sh MESH 2 4 1 1
#./runAzureusMeasurement_args.sh MESH 2 4 1 2
#./runAzureusMeasurement_args.sh MESH 2 4 1 3
#./runAzureusMeasurement_args.sh MESH 2 4 1 4
#./runAzureusMeasurement_args.sh MESH 2 4 1 5

#./runAzureusMeasurement_args.sh MESH 2 4 2 1
#./runAzureusMeasurement_args.sh MESH 2 4 2 2
#./runAzureusMeasurement_args.sh MESH 2 4 2 3
#./runAzureusMeasurement_args.sh MESH 2 4 2 4
#./runAzureusMeasurement_args.sh MESH 2 4 2 5

#./runAzureusMeasurement_args.sh MESH 2 4 3 1
#./runAzureusMeasurement_args.sh MESH 2 4 3 2
#./runAzureusMeasurement_args.sh MESH 2 4 3 3
#./runAzureusMeasurement_args.sh MESH 2 4 3 4
#./runAzureusMeasurement_args.sh MESH 2 4 3 5

#./runAzureusMeasurement_args.sh MESH 2 4 4 1
#./runAzureusMeasurement_args.sh MESH 2 4 4 2
#./runAzureusMeasurement_args.sh MESH 2 4 4 3
#./runAzureusMeasurement_args.sh MESH 2 4 4 4
#./runAzureusMeasurement_args.sh MESH 2 4 4 5

#./runAzureusMeasurement_args.sh MESH 2 4 5 1
#./runAzureusMeasurement_args.sh MESH 2 4 5 2
#./runAzureusMeasurement_args.sh MESH 2 4 5 3
#./runAzureusMeasurement_args.sh MESH 2 4 5 4
#./runAzureusMeasurement_args.sh MESH 2 4 5 5

#./runAzureusMeasurement_args.sh MESH 2 4 6 1
#./runAzureusMeasurement_args.sh MESH 2 4 6 2
#./runAzureusMeasurement_args.sh MESH 2 4 6 3
#./runAzureusMeasurement_args.sh MESH 2 4 6 4
#./runAzureusMeasurement_args.sh MESH 2 4 6 5

#./runAzureusMeasurement_args.sh MESH 2 4 7 1
#./runAzureusMeasurement_args.sh MESH 2 4 7 2
#./runAzureusMeasurement_args.sh MESH 2 4 7 3
#./runAzureusMeasurement_args.sh MESH 2 4 7 4
#./runAzureusMeasurement_args.sh MESH 2 4 7 5

#./runAzureusMeasurement_args.sh MESH 2 4 8 1
#./runAzureusMeasurement_args.sh MESH 2 4 8 2
#./runAzureusMeasurement_args.sh MESH 2 4 8 3
#./runAzureusMeasurement_args.sh MESH 2 4 8 4
#./runAzureusMeasurement_args.sh MESH 2 4 8 5

#./runAzureusMeasurement_args.sh MESH 2 4 9 1
#./runAzureusMeasurement_args.sh MESH 2 4 9 2
#./runAzureusMeasurement_args.sh MESH 2 4 9 3
#./runAzureusMeasurement_args.sh MESH 2 4 9 4
#./runAzureusMeasurement_args.sh MESH 2 4 9 5

#./runAzureusMeasurement_args.sh MESH 2 4 10 1
#./runAzureusMeasurement_args.sh MESH 2 4 10 2
#./runAzureusMeasurement_args.sh MESH 2 4 10 3
#./runAzureusMeasurement_args.sh MESH 2 4 10 4
#./runAzureusMeasurement_args.sh MESH 2 4 10 5

#./runAzureusMeasurement_args.sh MESH 2 4 11 1
#./runAzureusMeasurement_args.sh MESH 2 4 11 2
#./runAzureusMeasurement_args.sh MESH 2 4 11 3
#./runAzureusMeasurement_args.sh MESH 2 4 11 4
#./runAzureusMeasurement_args.sh MESH 2 4 11 5

#./runAzureusMeasurement_args.sh MESH 2 4 12 1
#./runAzureusMeasurement_args.sh MESH 2 4 12 2
#./runAzureusMeasurement_args.sh MESH 2 4 12 3
#./runAzureusMeasurement_args.sh MESH 2 4 12 4
#./runAzureusMeasurement_args.sh MESH 2 4 12 5

#./runAzureusMeasurement_args.sh MESH 2 4 13 1
#./runAzureusMeasurement_args.sh MESH 2 4 13 2
#./runAzureusMeasurement_args.sh MESH 2 4 13 3
#./runAzureusMeasurement_args.sh MESH 2 4 13 4
#./runAzureusMeasurement_args.sh MESH 2 4 13 5

#./runAzureusMeasurement_args.sh MESH 2 4 14 1
#./runAzureusMeasurement_args.sh MESH 2 4 14 2
#./runAzureusMeasurement_args.sh MESH 2 4 14 3
#./runAzureusMeasurement_args.sh MESH 2 4 14 4
#./runAzureusMeasurement_args.sh MESH 2 4 14 5

#./runAzureusMeasurement_args.sh MESH 2 4 15 1
#./runAzureusMeasurement_args.sh MESH 2 4 15 2
#./runAzureusMeasurement_args.sh MESH 2 4 15 3
#./runAzureusMeasurement_args.sh MESH 2 4 15 4
#./runAzureusMeasurement_args.sh MESH 2 4 15 5

#./runAzureusMeasurement_args.sh MESH 2 4 16 1
#./runAzureusMeasurement_args.sh MESH 2 4 16 2
#./runAzureusMeasurement_args.sh MESH 2 4 16 3
#./runAzureusMeasurement_args.sh MESH 2 4 16 4
#./runAzureusMeasurement_args.sh MESH 2 4 16 5

#./runAzureusMeasurement_args.sh MESH 2 4 17 1
#./runAzureusMeasurement_args.sh MESH 2 4 17 2
#./runAzureusMeasurement_args.sh MESH 2 4 17 3
#./runAzureusMeasurement_args.sh MESH 2 4 17 4
#./runAzureusMeasurement_args.sh MESH 2 4 17 5

#./runAzureusMeasurement_args.sh MESH 2 4 18 1
#./runAzureusMeasurement_args.sh MESH 2 4 18 2
#./runAzureusMeasurement_args.sh MESH 2 4 18 3
#./runAzureusMeasurement_args.sh MESH 2 4 18 4
#./runAzureusMeasurement_args.sh MESH 2 4 18 5





#./runAzureusMeasurement_args.sh MESH 2 5 1 1
#./runAzureusMeasurement_args.sh MESH 2 5 1 2
#./runAzureusMeasurement_args.sh MESH 2 5 1 3
#./runAzureusMeasurement_args.sh MESH 2 5 1 4
#./runAzureusMeasurement_args.sh MESH 2 5 1 5

#./runAzureusMeasurement_args.sh MESH 2 5 2 1
#./runAzureusMeasurement_args.sh MESH 2 5 2 2
#./runAzureusMeasurement_args.sh MESH 2 5 2 3
#./runAzureusMeasurement_args.sh MESH 2 5 2 4
#./runAzureusMeasurement_args.sh MESH 2 5 2 5

#./runAzureusMeasurement_args.sh MESH 2 5 3 1
#./runAzureusMeasurement_args.sh MESH 2 5 3 2
#./runAzureusMeasurement_args.sh MESH 2 5 3 3
#./runAzureusMeasurement_args.sh MESH 2 5 3 4
#./runAzureusMeasurement_args.sh MESH 2 5 3 5

#./runAzureusMeasurement_args.sh MESH 2 5 4 1
#./runAzureusMeasurement_args.sh MESH 2 5 4 2
#./runAzureusMeasurement_args.sh MESH 2 5 4 3
#./runAzureusMeasurement_args.sh MESH 2 5 4 4
#./runAzureusMeasurement_args.sh MESH 2 5 4 5

#./runAzureusMeasurement_args.sh MESH 2 5 5 1
#./runAzureusMeasurement_args.sh MESH 2 5 5 2
#./runAzureusMeasurement_args.sh MESH 2 5 5 3
#./runAzureusMeasurement_args.sh MESH 2 5 5 4
#./runAzureusMeasurement_args.sh MESH 2 5 5 5

#./runAzureusMeasurement_args.sh MESH 2 5 6 1
#./runAzureusMeasurement_args.sh MESH 2 5 6 2
#./runAzureusMeasurement_args.sh MESH 2 5 6 3
#./runAzureusMeasurement_args.sh MESH 2 5 6 4
#./runAzureusMeasurement_args.sh MESH 2 5 6 5

#./runAzureusMeasurement_args.sh MESH 2 5 7 1
#./runAzureusMeasurement_args.sh MESH 2 5 7 2
#./runAzureusMeasurement_args.sh MESH 2 5 7 3
#./runAzureusMeasurement_args.sh MESH 2 5 7 4
#./runAzureusMeasurement_args.sh MESH 2 5 7 5

#./runAzureusMeasurement_args.sh MESH 2 5 8 1
#./runAzureusMeasurement_args.sh MESH 2 5 8 2
#./runAzureusMeasurement_args.sh MESH 2 5 8 3
#./runAzureusMeasurement_args.sh MESH 2 5 8 4
#./runAzureusMeasurement_args.sh MESH 2 5 8 5

#./runAzureusMeasurement_args.sh MESH 2 5 9 1
#./runAzureusMeasurement_args.sh MESH 2 5 9 2
#./runAzureusMeasurement_args.sh MESH 2 5 9 3
#./runAzureusMeasurement_args.sh MESH 2 5 9 4
#./runAzureusMeasurement_args.sh MESH 2 5 9 5

#./runAzureusMeasurement_args.sh MESH 2 5 10 1
#./runAzureusMeasurement_args.sh MESH 2 5 10 2
#./runAzureusMeasurement_args.sh MESH 2 5 10 3
#./runAzureusMeasurement_args.sh MESH 2 5 10 4
#./runAzureusMeasurement_args.sh MESH 2 5 10 5

#./runAzureusMeasurement_args.sh MESH 2 5 11 1
#./runAzureusMeasurement_args.sh MESH 2 5 11 2
#./runAzureusMeasurement_args.sh MESH 2 5 11 3
#./runAzureusMeasurement_args.sh MESH 2 5 11 4
#./runAzureusMeasurement_args.sh MESH 2 5 11 5

#./runAzureusMeasurement_args.sh MESH 2 5 12 1
#./runAzureusMeasurement_args.sh MESH 2 5 12 2
#./runAzureusMeasurement_args.sh MESH 2 5 12 3
#./runAzureusMeasurement_args.sh MESH 2 5 12 4
#./runAzureusMeasurement_args.sh MESH 2 5 12 5

#./runAzureusMeasurement_args.sh MESH 2 5 13 1
#./runAzureusMeasurement_args.sh MESH 2 5 13 2
#./runAzureusMeasurement_args.sh MESH 2 5 13 3
#./runAzureusMeasurement_args.sh MESH 2 5 13 4
#./runAzureusMeasurement_args.sh MESH 2 5 13 5

#./runAzureusMeasurement_args.sh MESH 2 5 14 1
#./runAzureusMeasurement_args.sh MESH 2 5 14 2
#./runAzureusMeasurement_args.sh MESH 2 5 14 3
#./runAzureusMeasurement_args.sh MESH 2 5 14 4
#./runAzureusMeasurement_args.sh MESH 2 5 14 5

#./runAzureusMeasurement_args.sh MESH 2 5 15 1
#./runAzureusMeasurement_args.sh MESH 2 5 15 2
#./runAzureusMeasurement_args.sh MESH 2 5 15 3
#./runAzureusMeasurement_args.sh MESH 2 5 15 4
#./runAzureusMeasurement_args.sh MESH 2 5 15 5

#./runAzureusMeasurement_args.sh MESH 2 5 16 1
#./runAzureusMeasurement_args.sh MESH 2 5 16 2
#./runAzureusMeasurement_args.sh MESH 2 5 16 3
#./runAzureusMeasurement_args.sh MESH 2 5 16 4
#./runAzureusMeasurement_args.sh MESH 2 5 16 5

#./runAzureusMeasurement_args.sh MESH 2 5 17 1
#./runAzureusMeasurement_args.sh MESH 2 5 17 2
#./runAzureusMeasurement_args.sh MESH 2 5 17 3
#./runAzureusMeasurement_args.sh MESH 2 5 17 4
#./runAzureusMeasurement_args.sh MESH 2 5 17 5

#./runAzureusMeasurement_args.sh MESH 2 5 18 1
#./runAzureusMeasurement_args.sh MESH 2 5 18 2
#./runAzureusMeasurement_args.sh MESH 2 5 18 3
#./runAzureusMeasurement_args.sh MESH 2 5 18 4
#./runAzureusMeasurement_args.sh MESH 2 5 18 5


